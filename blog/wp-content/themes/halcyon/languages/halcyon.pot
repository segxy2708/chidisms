#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Halcyon\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-09-29 12:26+0000\n"
"POT-Revision-Date: Thu Jun 30 2016 16:35:03 GMT+0545 (Nepal Standard Time)\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: ..\n"
"X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;"
"__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;"
"_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;"
"esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;"
"esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2\n"
"X-Generator: Loco - https://localise.biz/"

#: 404.php:17
msgid "Oops! That page can&rsquo;t be found."
msgstr ""

#: 404.php:21
msgid ""
"It looks like nothing was found at this location. Maybe try one of the links "
"below or a search?"
msgstr ""

#: archive.php:49 index.php:53
msgid "Previous"
msgstr ""

#: archive.php:50 index.php:54
msgid "Next"
msgstr ""

#: archive.php:51 index.php:55
msgid "Page"
msgstr ""

#: comments.php:41
msgctxt "comments title"
msgid "1 Comment"
msgid_plural "%1$s Comments"
msgstr[0] ""
msgstr[1] ""

#: comments.php:60
msgid "Comment navigation"
msgstr ""

#: comments.php:63
msgid "Older Comments"
msgstr ""

#: comments.php:64
msgid "Newer Comments"
msgstr ""

#: comments.php:79
msgid "Comments are closed."
msgstr ""

#: comments.php:98
msgid "Name*"
msgstr ""

#: comments.php:101
msgid "Email*"
msgstr ""

#: comments.php:104
msgid "Website"
msgstr ""

#: comments.php:113
msgid "Send a Comment"
msgstr ""

#: comments.php:114
msgctxt "reply"
msgid "Send a Comment to %s"
msgstr ""

#: comments.php:115
msgid "Cancel Reply"
msgstr ""

#: comments.php:116
msgid "Post Comment"
msgstr ""

#: comments.php:119
msgid "Comment"
msgstr ""

#: comments.php:124
msgctxt "login url"
msgid "You must be <a href=\"%s\">logged in</a> to post a comment."
msgstr ""

#: comments.php:130
msgid ""
"Logged in as <a href=\"%1$s\">%2$s</a>. <a href=\"%3$s\" title=\"Log out of "
"this account\">Log out?</a>"
msgstr ""

#: comments.php:136
msgid ""
"<p class=\"comment-notes\"><span>Your email address will not be published."
"</span></p>"
msgstr ""

#: footer.php:49
msgid "Copyright &copy; "
msgstr ""

#: footer.php:50
msgctxt "home link"
msgid "%s"
msgstr ""

#: footer.php:51
msgctxt "author link"
msgid "%s"
msgstr ""

#: footer.php:51
msgid "https://raratheme.com/wordpress-themes/halcyon/"
msgstr ""

#: footer.php:52
msgctxt "WordPress link"
msgid "Powered by: %s"
msgstr ""

#: footer.php:52
msgid "https://wordpress.org/"
msgstr ""

#: functions.php:55
msgid "Primary"
msgstr ""

#: functions.php:145
msgid "Right Sidebar"
msgstr ""

#: functions.php:155
msgid "Footer One"
msgstr ""

#: functions.php:165
msgid "Footer Two"
msgstr ""

#: functions.php:175
msgid "Footer Three"
msgstr ""

#: header.php:34
msgctxt "Social link label"
msgid "<span>%s</span>"
msgstr ""

#: header.php:34
msgid "Find Me On"
msgstr ""

#: search.php:19
msgctxt "search query"
msgid "Search Results for: %s"
msgstr ""

#: searchform.php:11 searchform.php:12
msgctxt "label"
msgid "Search for:"
msgstr ""

#: searchform.php:12
msgctxt "placeholder"
msgid "Search"
msgstr ""

#: inc/customizer.php:18
msgid "Choose Post"
msgstr ""

#: inc/customizer.php:30
msgid "Default Settings"
msgstr ""

#: inc/customizer.php:31
msgid "Default section provided by wordpress customizer."
msgstr ""

#: inc/customizer.php:50
msgid "Slider Settings"
msgstr ""

#: inc/customizer.php:68
msgid "Enable Banner Slider"
msgstr ""

#: inc/customizer.php:86
msgid "Enable Slider Auto Transition"
msgstr ""

#: inc/customizer.php:104
msgid "Enable Slider Loop"
msgstr ""

#: inc/customizer.php:122
msgid "Enable Slider Control"
msgstr ""

#: inc/customizer.php:140
msgid "Enable Slider Pager"
msgstr ""

#: inc/customizer.php:158
msgid "Enable Slider Caption"
msgstr ""

#: inc/customizer.php:176
msgid "Choose Slider Animation"
msgstr ""

#: inc/customizer.php:180
msgid "Fade"
msgstr ""

#: inc/customizer.php:181
msgid "Slide"
msgstr ""

#: inc/customizer.php:198
msgid "Slider Speed"
msgstr ""

#: inc/customizer.php:216
msgid "Slider Pause"
msgstr ""

#: inc/customizer.php:226 inc/extras.php:177
msgid "Read Post"
msgstr ""

#: inc/customizer.php:234
msgid "Slider Read More"
msgstr ""

#: inc/customizer.php:245
msgid "Featured Posts Settings"
msgstr ""

#: inc/customizer.php:263
msgid "Enable Featured Post Section"
msgstr ""

#: inc/customizer.php:281
msgid "Featured Section Title"
msgstr ""

#: inc/customizer.php:299
msgid "Select Featured Post One"
msgstr ""

#: inc/customizer.php:318
msgid "Select Featured Post Two"
msgstr ""

#: inc/customizer.php:337
msgid "Select Featured Post Three"
msgstr ""

#: inc/customizer.php:349
msgid "Social Settings"
msgstr ""

#: inc/customizer.php:350
msgid "Leave blank if you do not want to show the social link."
msgstr ""

#: inc/customizer.php:368
msgid "Enable Social Links"
msgstr ""

#: inc/customizer.php:386 inc/extras.php:84 inc/widget-social-links.php:57 
#: inc/widget-social-links.php:106
msgid "Facebook"
msgstr ""

#: inc/customizer.php:404 inc/extras.php:86 inc/widget-social-links.php:59 
#: inc/widget-social-links.php:112
msgid "Twitter"
msgstr ""

#: inc/customizer.php:422 inc/extras.php:88 inc/widget-social-links.php:61 
#: inc/widget-social-links.php:118
msgid "Pinterest"
msgstr ""

#: inc/customizer.php:440 inc/extras.php:90 inc/widget-social-links.php:63 
#: inc/widget-social-links.php:124
msgid "Instagram"
msgstr ""

#: inc/customizer.php:458 inc/extras.php:92 inc/widget-social-links.php:65 
#: inc/widget-social-links.php:130
msgid "RSS"
msgstr ""

#: inc/customizer.php:476 inc/extras.php:94 inc/widget-social-links.php:67 
#: inc/widget-social-links.php:136
msgid "Dribble"
msgstr ""

#: inc/customizer.php:494 inc/extras.php:96 inc/widget-social-links.php:142
msgid "LinkedIn"
msgstr ""

#: inc/customizer.php:512 inc/extras.php:98 inc/widget-social-links.php:71 
#: inc/widget-social-links.php:148
msgid "Google Plus"
msgstr ""

#: inc/customizer.php:523
msgid "Custom CSS Settings"
msgstr ""

#: inc/customizer.php:541
msgid "Custom Css"
msgstr ""

#: inc/customizer.php:543
msgid "Put your custom CSS"
msgstr ""

#: inc/extras.php:144
msgctxt "comment author link"
msgid "<b class=\"fn\">%s</b>"
msgstr ""

#: inc/extras.php:147
msgid "Your comment is awaiting moderation."
msgstr ""

#. translators: 1: date, 2: time
#: inc/extras.php:156
msgctxt "comment date"
msgid "%1$s"
msgstr ""

#. translators: used between list items, there is a space after the comma
#: inc/extras.php:203 inc/extras.php:313 inc/template-tags.php:55
msgid ", "
msgstr ""

#: inc/info.php:11
msgid "Information Links"
msgstr ""

#: inc/info.php:21
msgid "Need help?"
msgstr ""

#: inc/info.php:22
msgid "View demo"
msgstr ""

#: inc/info.php:22 inc/info.php:23 inc/info.php:24 inc/info.php:25
msgid "here"
msgstr ""

#: inc/info.php:23
msgid "View documentation"
msgstr ""

#: inc/info.php:24
msgid "Support ticket"
msgstr ""

#: inc/info.php:25
msgid "More Details"
msgstr ""

#: inc/info.php:29
msgid "About Halcyon"
msgstr ""

#: inc/info.php:42
msgid "Pro Version"
msgstr ""

#. $id
#: inc/metabox.php:13
msgid "Sidebar Layout"
msgstr ""

#: inc/metabox.php:23
msgid "Right sidebar (default)"
msgstr ""

#: inc/metabox.php:28
msgid "No sidebar"
msgstr ""

#: inc/metabox.php:40
msgid "Choose Sidebar Template"
msgstr ""

#: inc/template-tags.php:24 inc/template-tags.php:27
msgctxt "post date"
msgid "%s"
msgstr ""

#: inc/template-tags.php:44
msgid "[ "
msgstr ""

#: inc/template-tags.php:44
msgid " ]"
msgstr ""

#: inc/template-tags.php:57
msgctxt "tag list"
msgid "Tags: %1$s"
msgstr ""

#: inc/template-tags.php:65
msgctxt "post author"
msgid "By "
msgstr ""

#: inc/template-tags.php:71
msgid "Leave a comment"
msgstr ""

#: inc/template-tags.php:71
msgid "1 Comment"
msgstr ""

#: inc/template-tags.php:71
msgctxt "no of comments"
msgid "% Comments"
msgstr ""

#. translators: %s: Name of current post
#. translators: %s: Name of current post
#: inc/template-tags.php:86 template-parts/content-page.php:35
msgctxt "Name of current post"
msgid "Edit %s"
msgstr ""

#. Base ID
#: inc/widget-featured-post.php:25
msgid "RARA: Featured Post"
msgstr ""

#: inc/widget-featured-post.php:26
msgid "A Featured Post Widget"
msgstr ""

#: inc/widget-featured-post.php:80
msgid "--choose--"
msgstr ""

#: inc/widget-featured-post.php:100
msgid "Posts"
msgstr ""

#: inc/widget-featured-post.php:110 inc/widget-popular-post.php:114 
#: inc/widget-recent-post.php:113 inc/widget-social-links.php:101
msgid "Title"
msgstr ""

#: inc/widget-featured-post.php:115
msgid "Profession"
msgstr ""

#: inc/widget-featured-post.php:121 inc/widget-popular-post.php:130 
#: inc/widget-recent-post.php:129
msgid "Show Post Thumbnail"
msgstr ""

#. Base ID
#: inc/widget-popular-post.php:25
msgid "RARA: Popular Post"
msgstr ""

#: inc/widget-popular-post.php:26
msgid "A Popular Post Widget"
msgstr ""

#: inc/widget-popular-post.php:40 inc/widget-popular-post.php:105 
#: inc/widget-popular-post.php:154
msgid "Popular Posts"
msgstr ""

#: inc/widget-popular-post.php:75 inc/widget-recent-post.php:74
msgctxt "post date"
msgid "%1$s"
msgstr ""

#: inc/widget-popular-post.php:84 inc/widget-recent-post.php:83 
#: template-parts/content.php:63
msgid "Read More"
msgstr ""

#: inc/widget-popular-post.php:119 inc/widget-recent-post.php:118
msgid "Short Description"
msgstr ""

#: inc/widget-popular-post.php:124 inc/widget-recent-post.php:123
msgid "Number of Posts"
msgstr ""

#: inc/widget-popular-post.php:135 inc/widget-recent-post.php:134
msgid "Show Post Date"
msgstr ""

#. Base ID
#: inc/widget-recent-post.php:25
msgid "RARA: Recent Post"
msgstr ""

#: inc/widget-recent-post.php:26
msgid "A Recent Post Widget"
msgstr ""

#: inc/widget-recent-post.php:40 inc/widget-recent-post.php:104 
#: inc/widget-recent-post.php:153
msgid "Recent Posts"
msgstr ""

#. Base ID
#: inc/widget-social-links.php:25
msgid "RARA: Social Links"
msgstr ""

#: inc/widget-social-links.php:26
msgid "A Social Links Widget"
msgstr ""

#: inc/widget-social-links.php:40 inc/widget-social-links.php:88 
#: inc/widget-social-links.php:168
msgid "Social"
msgstr ""

#: inc/widget-social-links.php:69
msgid "Linkedin"
msgstr ""

#: inc/widget-social-links.php:108 inc/widget-social-links.php:114 
#: inc/widget-social-links.php:120 inc/widget-social-links.php:126 
#: inc/widget-social-links.php:132 inc/widget-social-links.php:138 
#: inc/widget-social-links.php:144 inc/widget-social-links.php:150
msgid "Leave blank if you do not want to show."
msgstr ""

#: template-parts/content-none.php:14
msgid "Nothing Found"
msgstr ""

#: template-parts/content-none.php:21
msgctxt "add new post link"
msgid ""
"Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr ""

#: template-parts/content-none.php:25
msgid ""
"Sorry, but nothing matched your search terms. Please try again with some "
"different keywords."
msgstr ""

#: template-parts/content-none.php:31
msgid ""
"It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps "
"searching can help."
msgstr ""

#: template-parts/content-page.php:24 template-parts/content.php:52
msgid "Pages:"
msgstr ""

#: template-parts/content.php:37
msgctxt "Name of current post"
msgid "Continue reading %s <span class=\"meta-nav\">&rarr;</span>"
msgstr ""

msgid "Halcyon"
msgstr ""

#. Description of the theme
msgid ""
"Halcyon is a modern and clean WordPress theme designed for a blog or a "
"website. The large featured slider with advance slider options makes this "
"theme a great choice for photographers, or any users who want to share their "
"visuals in a bold way. The three footer widget areas offer plenty of space "
"for any secondary information, and striking typography makes the content "
"look stunning. The Theme has a built-in Recent Posts widget, Popular Posts "
"widget, Social Link Widget and Author widget. It features full width page "
"option, SEO friendly structure, custom logo support and social media links. "
"The theme is responsive and scales beautifully with various screen sizes "
"while maintaining the text readability and image quality. The theme is also "
"translation ready."
msgstr ""

#. Theme URI of the theme
msgid "http://raratheme.com/wordpress-themes/halcyon/"
msgstr ""

msgid "Rara Theme"
msgstr ""

#. Author URI of the theme
msgid "http://raratheme.com/"
msgstr ""
