<?php
/**
 * The template for displaying comments.
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Halcyon
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div class="author-section">
	<p><?php echo get_avatar( get_the_author_meta( 'ID' ), 125 ); ?></p>
	<div class="text-holder">
		<?php echo wpautop( '<span>' . esc_html( get_the_author_meta( 'display_name' ) ) . '</span>' ) ; 
            echo wpautop( esc_html( get_the_author_meta( 'description' ) ) );
        ?>
	</div>
</div>



	<?php
	// You can start editing here -- including this comment!
	if ( have_comments() ) : ?>
    <div id="comments" class="comments-area">
		<h3 class="comments-title">
			<?php
				printf( // WPCS: XSS OK.
					esc_html( _nx( '1 Comment', '%1$s Comments', get_comments_number(), 'comments title', 'halcyon' ) ),
					number_format_i18n( get_comments_number() )
				);
			?>
		</h3>

		<ol class="comment-list">
			<?php
				wp_list_comments( array(
					'style'      => 'ol',
					'short_ping' => true,
                    'callback'   => 'halcyon_theme_comment',
                    'avatar_size'=> 85,
				) );
			?>
		</ol><!-- .comment-list -->

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
		<nav id="comment-nav-below" class="navigation comment-navigation" role="navigation">
			<h2 class="screen-reader-text"><?php esc_html_e( 'Comment navigation', 'halcyon' ); ?></h2>
			<div class="nav-links">

				<div class="nav-previous"><?php previous_comments_link( esc_html__( 'Older Comments', 'halcyon' ) ); ?></div>
				<div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments', 'halcyon' ) ); ?></div>

			</div><!-- .nav-links -->
		</nav><!-- #comment-nav-below -->
		<?php
		endif; // Check for comment navigation.
        ?>
    </div><!-- #comments -->
	<?php
    endif; // Check for have_comments().


	// If comments are closed and there are comments, let's leave a little note, shall we?
	if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>

		<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'halcyon' ); ?></p>
	<?php
	endif;

	?>



<div class="comment-form">
    <div class="comments-area form">
    <?php 
    
    //https://codex.wordpress.org/Function_Reference/comment_form
    $commenter = wp_get_current_commenter();
    $req = get_option( 'require_name_email' );
    $aria_req = ( $req ? " aria-required='true'" : '' );
    
    $fields =  array(
        'author' =>
            '<p class="comment-form-author"><input id="author" name="author" type="text" placeholder="' . esc_attr__( 'Name*', 'halcyon' ) . '" value="' . esc_attr( $commenter['comment_author'] ) .
            '" size="30"' . $aria_req . ' /></p>',
        'email' =>
            '<p class="comment-form-email"><input id="email" name="email" type="text" placeholder="' . esc_attr__( 'Email*', 'halcyon' ) . '" value="' . esc_attr(  $commenter['comment_author_email'] ) .
            '" size="30"' . $aria_req . ' /></p>',
        'url' =>
            '<p class="comment-form-url"><input id="url" name="url" type="text" placeholder="' . esc_attr__( 'Website', 'halcyon' ) . '" value="' . esc_attr( $commenter['comment_author_url'] ) .
            '" size="30" /></p>',
    );
        
    $comment_arg = array(
        'id_form'           => 'commentform',
        'id_submit'         => 'submit',
        'class_submit'      => 'submit',
        'name_submit'       => 'submit',
        'title_reply'       => __( 'Send a Comment', 'halcyon' ),
        'title_reply_to'    => _x( 'Send a Comment to %s', 'reply', 'halcyon' ),
        'cancel_reply_link' => __( 'Cancel Reply', 'halcyon' ),
        'label_submit'      => __( 'Post Comment', 'halcyon' ),
        'format'            => 'xhtml',

        'comment_field' =>  '<p class="comment-form-comment"><textarea id="comment" name="comment" placeholder="' . esc_attr__( 'Comment', 'halcyon' ) . '" cols="45" rows="8" aria-required="true">' .
            '</textarea></p>',

        'must_log_in' => '<p class="must-log-in">' .
        sprintf(
        _x( 'You must be <a href="%s">logged in</a> to post a comment.', 'login url', 'halcyon' ),
        wp_login_url( apply_filters( 'the_permalink', get_permalink() ) )
        ) . '</p>',

        'logged_in_as' => '<p class="logged-in-as">' .
        sprintf(
        __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>', 'halcyon' ),
        admin_url( 'profile.php' ),
        $user_identity,
        wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) )
        ) . '</p>',            
        
        'comment_notes_before' => __( '<p class="comment-notes"><span>Your email address will not be published.</span></p>', 'halcyon' ),
        'comment_notes_after' => '',

        'fields' => apply_filters( 'comment_form_default_fields', $fields ),
    );
    
    comment_form( $comment_arg );?>
    </div>
</div>