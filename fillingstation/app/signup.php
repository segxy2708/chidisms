<?php
header("Access-Control-Allow-Origin: *");
header('Content-type: application/json');


require 'mail/smtp.php';

// script to connect to database
$host = "localhost";
$user = "siscomed_signup";      //$user = "root";
$password = "En!0s@b";            //$paswd=""
$db = "siscomed_signup";  //$db = "kidszone";
$conn = new mysqli($host, $user, $password, $db);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

date_default_timezone_get('Africa/Lagos');
$dateenter = date('d-m-Y');
//$dateenter = date('d-m-Y h:i A');

//$lon1=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['longitude'])));
//$lat1=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['latitude'])));
//$unit = 'K';

function distance($lat1, $lon1, $lat2, $lon2, $unit) {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
    return ($miles * 1.609344);
  } else if ($unit == "N") {
      return ($miles * 0.8684);
    } else {
        return $miles;
      }
}
	


//script to signup new user
if(isset($_REQUEST['phone']))
{
$fullname=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['name'])));
$email=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['email'])));
$phone=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['phone'])));
$password=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['password'])));
$passwd=password_hash($password, PASSWORD_DEFAULT);
$stationaddress=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['address']))).' ';
$stationaddress.=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['city']))).' ';
$stationaddress.=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['stationstate']))).' ';
$stationaddress.=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['country'])));

$station_address = mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['address'])));
$stationcity = mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['city'])));
$stationstate = mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['stationstate'])));


//Google Map geocode the address
$json_a = json_decode($getlat, true);

$streetaddress= str_replace (" ", "+", $stationaddress);
   $details_url = "https://maps.googleapis.com/maps/api/geocode/json?address=" .$streetaddress. "&key=AIzaSyCsSe3OmkyY1Ts2pkJyXFWMAW7zYuqTaOA";

   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $details_url);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
   $geoloc = json_decode(curl_exec($ch), true);
 
 $reglatitude = $geoloc['results'][0]['geometry']['location']['lat']; // get lat for json
 $reglongitude = $geoloc['results'][0]['geometry']['location']['lng']; // get ing for json

$latitude =mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['lati'])));
$longitude =mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['long'])));
$category =mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['category'])));
$result = $conn->query("select * from signup where phone='$phone'");
$signupuser=$result->num_rows;
if($signupuser >= '1')
{
	$exist= array("exist"=>'exist');
	echo json_encode($exist);

}
else
{
$date=date("d-m-y h:i:s");
$q=$conn->query("insert into `signup` (`reg_date`,`name`,`email`,`password`, `phone`, `stationaddress`, `stationcity`, `stationstate`, `latitude`, `longitude`, `category`) values 
('$dateenter','$fullname','$email','$passwd', '$phone', '$station_address', '$stationcity', '$stationstate', '$reglatitude', '$reglongitude', '$category')");
if($q)
{
$success= array("success"=>'success');
	echo json_encode($success);
}
else
{
$failed= array("failed"=>'failed');
	echo json_encode($failed);
}
}


}

//script to authenticate and login user
if(isset($_REQUEST['phone2']))
{
$phone =mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['phone2'])));
$password=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['password2'])));
//$passwd=password_hash($password, PASSWORD_DEFAULT);
$loginuser= $conn->query("select * from signup where phone='$phone'");
$login=$loginuser->num_rows;
//
$result=$loginuser->fetch_assoc();
$password_verify=$result['password'];
//echo $password_verify;
if($login=='1' && password_verify($password,$password_verify))
{

 	$array[] = $result;
	$json = json_encode($array);
	echo $json;

}
else
{
$false = array("false"=>'false');
	echo json_encode($false);

}

}

//script to list filling station close-by
if(isset($_REQUEST['latitude']))
{

$log= $conn->query("
SELECT s.reg_id, s.name, s.stationaddress,s.stationcity,s.stationstate, s.phone, s.longitude, s.latitude, f.fuelprice, f.paymethod, f.dieselprice,f.keroseneprice, f.status_update, f.updated
FROM signup as s
INNER JOIN fuel_update as f
WHERE s.category='station' AND s.reg_id = f.station_id  
LIMIT 100
");
echo $conn->error;

function cmp($a,$b)
	{
  return $a["distance"] - $b["distance"];

};

while ($result=$log->fetch_assoc()){
	$lon1=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['longitude'])));
        $lat1=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['latitude'])));
        $unit = 'k';

	
	$lon2= $result['longitude'];
	$lat2= $result['latitude'];
	$distance = distance($lat1, $lon1, $lat2, $lon2, $unit);
	$dist=array('distance'=>$distance);
	$result = array_merge($result,$dist);
	$array[] = $result;
	
	 };
    usort($array, "cmp");

	$json = json_encode($array);
	 //print_r($array);
	echo $json;

}

//script to fuel update by filling station
if(isset($_REQUEST['fuelprice']))
{
$userid =mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['fstationid'])));
$fuelprice =mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['fuelprice'])));
$status=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['status'])));
$column=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['column'])));
$paymethod=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['paymethod'])));

$fuel = $conn->query("select station_id,status_update from fuel_update  where station_id = '$userid'");
$fetch = $fuel->fetch_array();
$status_update = $fetch['status_update'];
/*
$fuelnotify = $conn->query("select station_id,name from signup  where station_id = '$userid'");
$fetchnotify = $fuelnotify->fetch_array();
$messageprice = 'New Price for '.$column.' is '.$fuelprice;
$fuelservernotify = $conn->query("INSERT INTO `notification`(`messagetitle`,`message`,`messagedate`) VALUES('Price Update from $fetchnotify['name']', '$messageprice', '$dateenter')");*/

 
 //$status.$status_update;
	function status($status,$status_update,$column){
	//fuel update
	if($status == 2 && $column == 'fuelprice'){
	 $commodity = 'f';
	 
	 //check if status is currently selling. If selling, no need updating the status
	if(strpos($status_update,$commodity) !== false){
		$update = $status_update;
		return $update;
		
	}else {
		//add status
		$commodity = 'f';
		$update = $status_update.$commodity;
		return $update;
	}
	 
	}
	else if($status == 1 && $column == 'fuelprice'){
		//not selling
		$commodity = 'f';
		$update = str_replace($commodity,"",$status_update);
	 //check if status is currently selling. If selling, remove it
	 return $update;
	}
	
	//diesel update
	if($status == 2 && $column == 'dieselprice'){
	 $commodity = 'd';
	 
	 //check if status is currently selling. If selling, no need updating the status
	if(strpos($status_update,$commodity) !== false){
		$update = $status_update;
		return $update;
		
	}else {
		//add status
		$commodity = 'd';
		$update = $status_update.$commodity;
		return $update;
	}
	 
	}
	else if($status == 1 && $column == 'dieselprice'){
		//not selling
		$commodity = 'd';
		$update = str_replace($commodity,"",$status_update);
	 //check if status is currently selling. If selling, remove it
	 return $update;
	}
	
	//kerosene update
	if($status == 2 && $column == 'keroseneprice'){
	 $commodity = 'k';
	 
	 //check if status is currently selling. If selling, no need updating the status
	if(strpos($status_update,$commodity) !== false){
		$update = $status_update;
		return $update;
		
	}else {
		//add status
		$commodity = 'k';
		$update = $status_update.$commodity;
		return $update;
	}
	 
	}
	else if($status == 1 && $column == 'keroseneprice'){
		//not selling
		$commodity = 'k';
		$update = str_replace($commodity,"",$status_update);
	 //check if status is currently selling. If selling, remove it
	 return $update;
	}
	
	
	};
	$update = status($status,$status_update,$column) ;
 
define('table_column',$column);
	
$fuelprice_update = $fuel->num_rows;
if($fuelprice_update != 0)
{
	$updates = $conn->query("UPDATE fuel_update SET ".table_column." = $fuelprice, status_update = '$update', updated='$dateenter', paymethod =IF(LENGTH('$paymethod')='', paymethod, '$paymethod') WHERE station_id=$userid ");
	
	echo $conn->error;
	if($updates){$fuel = array("update"=>'updated');
	$fuelupdated =  json_encode($fuel);
	echo $fuelupdated; }
	else 
	echo "Error updating record: " . $conn->error;
	
	
	
}
else
{

	$updatexec = $conn->query("INSERT INTO `fuel_update`(`".table_column."`,`status_update`,`station_id`, `updated`) VALUES('$fuelprice', '$update', '$userid', '$dateenter') ");
	echo $conn->error;
	
	$fuel = array("update"=>'updated');
	$fuelnotupdated = json_encode($fuel);
	echo $fuelnotupdated; 
}

}

//script to update users information
if(isset($_REQUEST['setupdate']))
{
		
$settingstationid=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['sstationid'])));
$settingname=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['settingname'])));
$settingemail=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['settingemail'])));
$settingpassword=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['settingpassword'])));
$settingpasswd = password_hash($settingpassword, PASSWORD_DEFAULT);
$settingaddress=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['settingaddress'])));
$settingcategory=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['settingcategory'])));
$settinglong=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['settinglong'])));
$settinglati=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['settinglati'])));

if($settingcategory=='user'){
	$settinglong='';
	$settinglati='';
};

$updates = $conn->query("UPDATE signup SET name =IF(LENGTH('$settingname')='', name, '$settingname'), email = IF(LENGTH('$settingemail')='', email, '$settingemail'), stationaddress = IF(LENGTH('$settingaddress')='', stationaddress, '$settingaddress'), latitude = IF(LENGTH('$settinglati')='', latitude, '$settinglati'), longitude = IF(LENGTH('$settinglong')='', longitude, '$settinglong'), password = IF(LENGTH('$settingpasswd')='', password, '$settingpasswd') WHERE reg_id='$settingstationid' ");
	//echo "Error updating record: " . $conn->error;
	if($updates){
	$updatesettings = array("update"=>'updated');
	echo json_encode($updatesettings);
	}else{
	$failedupdate = array("update"=>'failed');
	echo json_encode($failedupdate);
	}
	
}


//script to bookmark
if(isset($_REQUEST['bookmarkid']))
{
		
$bookmarkid=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['bookmarkid'])));
$userid=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['userid'])));
$bookstatus = 'booked';

$checkbookmark = "SELECT stationid FROM bookmarked WHERE stationid = '$bookmarkid' AND status = 'booked'";
$bookmarkquery = $conn ->query($checkbookmark);
$bookresult = $bookmarkquery->num_rows;
	if($bookresult=='1'){
		$deletebookmark = "DELETE FROM bookmarked WHERE stationid = '$bookmarkid'";
		$bookmarkquery = $conn ->query($deletebookmark);
		$deletedbookmark = array("komot"=>'komoted');
		echo json_encode($deletedbookmark);
	}else{
		$booking = $conn->query("INSERT INTO `bookmarked`(`userid`,`stationid`,`status`) VALUES('$userid', '$bookmarkid', '$bookstatus')");
		//echo "Error updating record: " . $conn->error;
		if($booking){
		$bookupdate = array("bookmarked"=>'booked');
		echo json_encode($bookupdate );
		}else{
		$failedbookup = array("fail"=>'failed');
		echo json_encode($failedbookup);
	}
	}
	}
	
if(isset($_REQUEST['favuserid']))
{
$userid=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['favuserid'])));

$favoritequery = $conn ->query(
		"SELECT s.reg_id, s.name, s.stationaddress, s.phone, s.longitude, s.latitude, b.userid, b.stationid, f.fuelprice, f.paymethod, f.dieselprice, f.keroseneprice, f.status_update, f.updated
		FROM signup as s
		INNER JOIN bookmarked as b
		INNER JOIN fuel_update as f
		WHERE s.category='station' AND b.userid = '$userid'  AND s.reg_id = b.stationid AND s.reg_id = f.station_id 
		");
	//echo "Error fav record: " . $conn->error;
	$favcount = $favoritequery->num_rows;
	if($favcount>='1'){
	
	while ($favresult=$favoritequery->fetch_assoc()){
$lon1=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['favlongitude'])));
$lat1=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['favlatitude'])));
$unit = 'k';

	$lat2= $favresult['longitude'];
	$lon2= $favresult['latitude'];
	$favdistance = distance($lat1, $lon1, $lat2, $lon2, $unit);
	$favdist=array('distance'=>$favdistance );
	$favresult= array_merge($favresult,$favdist);
	
	$favarray[] = $favresult;
	$favjson = json_encode($favarray);
	 };
	echo $favjson;
	}else{
		$nofav = array("nofav"=>'nofavorite');
		echo json_encode($nofav);
	}
	}
	
if(isset($_REQUEST['stationid']))
{
$stationid=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['stationid'])));
$ratinguser=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['ratinguser'])));

//Get the ratings and number of voters
$rating = $conn ->query(
		"SELECT (COUNT(CASE WHEN availability = 1 THEN 1 END ) * 1) a1, (COUNT(CASE WHEN cost = 1 THEN 1 END ) * 1) c1, (COUNT(CASE WHEN accurate_pump = 1 THEN 1 END) * 1) ap1, (COUNT(CASE WHEN customer_service = 1 THEN 1 END) * 1) cs1, (COUNT(CASE WHEN availability = 2 THEN 1 END ) * 2) a2, (COUNT(CASE WHEN cost = 2 THEN 1 END ) * 2) c2, (COUNT(CASE WHEN accurate_pump = 2 THEN 1 END) * 2) ap2, (COUNT(CASE WHEN customer_service = 2 THEN 1 END) * 2) cs2,(COUNT(CASE WHEN availability = 3 THEN 1 END ) * 3) a3, (COUNT(CASE WHEN cost = 3 THEN 1 END ) * 3) c3, (COUNT(CASE WHEN accurate_pump = 3 THEN 1 END) * 3) ap3, (COUNT(CASE WHEN customer_service = 3 THEN 1 END) * 3) cs3,(COUNT(CASE WHEN availability = 4 THEN 1 END ) * 4) a4, (COUNT(CASE WHEN cost = 4 THEN 1 END ) * 4) c4, (COUNT(CASE WHEN accurate_pump = 4 THEN 1 END) * 4) ap4, (COUNT(CASE WHEN customer_service = 4 THEN 1 END) * 4) cs4,(COUNT(CASE WHEN availability = 5 THEN 1 END ) * 5) a5, (COUNT(CASE WHEN cost = 5 THEN 1 END ) * 5) c5, (COUNT(CASE WHEN accurate_pump = 5 THEN 1 END) * 5) ap5, (COUNT(CASE WHEN customer_service = 5 THEN 1 END) * 5) cs5, COUNT(CASE WHEN availability = 1 THEN 1 END )  ta1, COUNT(CASE WHEN cost = 1 THEN 1 END )  tc1, COUNT(CASE WHEN accurate_pump = 1 THEN 1 END) tap1, COUNT(CASE WHEN customer_service = 1 THEN 1 END) tcs1, COUNT(CASE WHEN availability = 2 THEN 1 END ) ta2, COUNT(CASE WHEN cost = 2 THEN 1 END ) tc2, COUNT(CASE WHEN accurate_pump = 2 THEN 1 END) tap2, COUNT(CASE WHEN customer_service = 2 THEN 1 END) tcs2,COUNT(CASE WHEN availability = 3 THEN 1 END ) ta3, COUNT(CASE WHEN cost = 3 THEN 1 END ) tc3, COUNT(CASE WHEN accurate_pump = 3 THEN 1 END) tap3, COUNT(CASE WHEN customer_service = 3 THEN 1 END) tcs3,COUNT(CASE WHEN availability = 4 THEN 1 END ) ta4, COUNT(CASE WHEN cost = 4 THEN 1 END ) tc4, COUNT(CASE WHEN accurate_pump = 4 THEN 1 END) tap4, COUNT(CASE WHEN customer_service = 4 THEN 1 END) tcs4,COUNT(CASE WHEN availability = 5 THEN 1 END ) ta5, COUNT(CASE WHEN cost = 5 THEN 1 END ) tc5, COUNT(CASE WHEN accurate_pump = 5 THEN 1 END) tap5, COUNT(CASE WHEN customer_service = 5 THEN 1 END) tcs5
		FROM ratings WHERE station_id = $stationid ");
	$ratingresult=$rating->fetch_assoc();
	echo $conn->error;
	$total_availability = $ratingresult['a1']+$ratingresult['a2']+$ratingresult['a3']+$ratingresult['a4']+$ratingresult['a5'];
        $total_cost = $ratingresult['c1']+$ratingresult['c2']+$ratingresult['c3']+$ratingresult['c4']+$ratingresult['c5'];
        $total_accurate_pump = $ratingresult['ap1']+$ratingresult['ap2']+$ratingresult['ap3']+$ratingresult['ap4']+$ratingresult['ap5'];
        $total_customer_service = $ratingresult['cs1']+$ratingresult['cs2']+$ratingresult['cs3']+$ratingresult['cs4']+$ratingresult['cs5'];

       //add the total number of voters
        $users_availability  = $ratingresult['ta1']+$ratingresult['ta2']+$ratingresult['ta3']+$ratingresult['ta4']+$ratingresult['ta5'];
        $users_cost = $ratingresult['tc1']+$ratingresult['tc2']+$ratingresult['tc3']+$ratingresult['tc4']+$ratingresult['tc5'];
        $users_accurate_pump = $ratingresult['tap1']+$ratingresult['tap2']+$ratingresult['tap3']+$ratingresult['tap4']+$ratingresult['tap5'];
        $users_customer_service = $ratingresult['tcs1']+$ratingresult['tcs2']+$ratingresult['tcs3']+$ratingresult['tcs4']+$ratingresult['tcs5'];

        $availability = round($total_availability / $users_availability);
         $cost = round($total_cost / $users_cost);
       $accurate_pump = round($total_accurate_pump / $users_accurate_pump);
        $customer_service = round($total_customer_service / $users_customer_service);
        $cummulative_rating = round(($cost + $accurate_pump + $availability + $customer_service) / 4, 1);
        $cummulative = round(($cost + $accurate_pump + $availability + $customer_service) / 4);
       
        $ratingresultarray = array('cummulative' => $cummulative, 'cummulative_rating' => $cummulative_rating, 'voters_availability' => $users_availability , 'voters_cost' => $users_cost, 'voters_accurate_pump'=>$users_accurate_pump, 'voters_customer_service'=>$users_customer_service, 'availability'=>$availability, 'cost'=>$cost, 'accurate_pump'=>$accurate_pump,  'customer_service'=>$customer_service );


$stationquery = $conn ->query(
		"SELECT s.reg_id, s.name, s.stationaddress, s.phone, s.longitude, s.latitude, c.userid,c.commenter, c.stationid, c.comment,c.dateline, f.fuelprice,f.paymethod,f.dieselprice,f.keroseneprice, f.status_update, f.updated
		FROM signup as s
		INNER JOIN userscomments as c
		INNER JOIN fuel_update as f
		WHERE  s.reg_id = c.stationid AND s.reg_id = f.station_id AND c.stationid = '$stationid'");
		
$stationcount = $stationquery->num_rows;

//getting the favorite
$bukx = $conn ->query("SELECT * FROM bookmarked WHERE stationid='$stationid' AND userid='$ratinguser'");
$countbukx = $bukx->num_rows;
if($countbukx == '1')
{
$favoritearray=array('favorite' => 'bookmarked');
}else{
$favoritearray=array('favorite' => 'nobookmarked');
}
;


$lon1=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['stationlongitude'])));
$lat1=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['stationlatitude'])));
$unit = 'k';

	if($stationcount >='1'){
	
	
	
	while ($stationresult=$stationquery->fetch_assoc()){
	
	



	$lon2= $stationresult['longitude'];
	$lat2= $stationresult['latitude'];
	$stationdistance = distance($lat1, $lon1, $lat2, $lon2, $unit);
	$stationdist=array('distance'=>$stationdistance);
	$stationresult= array_merge($stationresult,$stationdist,$ratingresultarray,$favoritearray);
	
	$stationarray[] = $stationresult;
	$stationjson = json_encode($stationarray);
	 };
	echo $stationjson;
	}else{

        //no comment
		$zerocomm = $conn ->query(
	"SELECT s.reg_id, s.name, s.stationaddress, s.phone, s.longitude, s.latitude, f.fuelprice,f.paymethod,f.dieselprice,f.keroseneprice, f.status_update, f.updated
		FROM signup as s
		INNER JOIN fuel_update as f
		WHERE f.station_id = '$stationid'  AND s.reg_id = f.station_id
		");
		//echo $conn->error;


		$zecomm = $zerocomm->fetch_assoc();
		$lon2= $zecomm['longitude'];
		$lat2= $zecomm['latitude'];
		$zerostationdistance = distance($lat1, $lon1, $lat2, $lon2, $unit);
		$zerostationdist=array('distance'=>$zerostationdistance );
		$nocomment = array("nocomment"=>'No Comment');
		$zerostationresult= array_merge($zecomm,$zerostationdist,$nocomment,$ratingresultarray,$favoritearray);
		$stationarray[] = $zerostationresult;
		$zstationjson = json_encode($stationarray);
		echo $zstationjson;
		
	}
	}
	


if(isset($_REQUEST['comment']))
{
$comment=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['comment'])));
$cuserid=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['cuserid'])));
$cstationid=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['cstationid'])));

$commenter = $conn ->query(
		"SELECT name 
		FROM signup 
		WHERE reg_id = '$cuserid' 
		");
$commenter1 = $commenter->fetch_array();
$commenter2 = $commenter1['name'];

//$commenter3 = array("commenter"=>$commenter2);
	//json_encode($commenter3);
$number = $commenter->num_rows;

	if($number=='1'){
	$insertcomment =  $conn->query("INSERT INTO `userscomments`(`userid`, `stationid`, `commenter`, `comment`, `dateline`) VALUES('$cuserid', '$cstationid', '$commenter2', '$comment', '$dateenter')");
	$comment = htmlentities($comment);
	$time = date("d-m-Y");
	 $done = array("done"=>'Done', "commenter"=>$commenter2,"comment"=>$comment, 'time'=>$time);
	 echo json_encode($done);
	}else{
	$nodone = array("notdone"=>'not done');
	echo json_encode($nodone);
	}
	}
	

if(isset($_REQUEST['passwordresetemail']))
{
$passwordresetemail=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['passwordresetemail'])));
//$passwordresetemail = filter_var($_POST["passwordresetemail"], FILTER_VALIDATE_EMAIL);
//$passworduserid=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['passworduserid'])));

$passwordreset = $conn ->query(
		"SELECT email,name,phone
		FROM signup 
		WHERE email = '$passwordresetemail' 
		");
$passwordreset1 = $passwordreset->fetch_array();
$passwordname = $passwordreset1['name'];
$passwordemail = $passwordreset1['email'];
$passwordphone = $passwordreset1['phone'];
$conn->error;

//$commenter3 = array("commenter"=>$commenter2);
	//json_encode($commenter3);
$number = $passwordreset->num_rows;
//echo $number;
	if($number=='1'){
// Create the unique user password reset key
		$link= password_hash($passwordemail,PASSWORD_DEFAULT);

		// Create a url which we will direct them to reset their password
		$pwrurl = "http://www.siscomedia.com.ng/loginapp/reset_password.php?q=".$link;
		
		//recipient of email
		$mail->addAddress($passwordemail, $passwordname);
		
		// Mail them their key
		$body = "Dear ".ucfirst($passwordname).",<br><br>If this e-mail does not apply to you please ignore it. It appears that you have requested a password reset from our Filling Station GeoLocation Mobile App www.yoursitehere.com.ng<br><br> <strong>Contact Phone: $passwordphone <br>Contact Email: $passwordemail</strong>  <br><br>To reset your password, please click the link below. If you cannot click it, please paste it into your web browser's address bar.<br><br>" . $pwrurl . "<br><br>Thanks,<br>The Administrator";
		
		$mail->IsHTML(true);
		$mail->Body=$body;
		
	$sentmsg = $mail->send();
		
		if (!$sentmsg) {
   	 $emailerr = array("mailerdeamon"=>'Mailer Deamon'. $mail->ErrorInfo);
	$emailerror = json_encode($emailerr);
          echo $emailerror;

	} else {
   	
       $sent = array("done"=>'Done');

	echo json_encode($sent);
	}
		
	}else{
	$emailsearch = array("notfound"=>'not found');
	echo json_encode($emailsearch);

	}
	}

    
// Was the password reset form submitted?
if (isset($_REQUEST["resetemailform"]))
{
	// Gather the post data
	$resetemail = mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST["resetemailform"])));
	$password = mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST["resetpasswordform"])));
	$confirmpassword = mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST["resetconfirmpasswordform"])));
	$hash = mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST["q"])));

	// Generate the reset key
	//$resetkey = password_hash($resetemail,PASSWORD_DEFAULT);
	$sendback = array('hash' => $hash, 'email' => $resetemail);
	//echo json_encode($sendback);
	// Does the new reset key match the old one?
	if (password_verify($resetemail, $hash))
	{
	
		if ($password == $confirmpassword)
		{
		
			//hash and secure the password
			$password = password_hash($password, PASSWORD_DEFAULT);

			// Update the user's password
			$query = $conn->query("UPDATE signup SET password= '$password', date_modified = '$dateenter' WHERE email = '$resetemail' ");
$reset_successful = array("success" => "success");				
echo json_encode($reset_successful);
		}
		else{
		
$reset_mismatch = array("mismatch" => "mismatch");				
echo json_encode($reset_mismatch);	
}
	}
	else{
	
$reset_invalid = array("invalid" => "invalid");				
echo json_encode($reset_invalid);
		}
}


//script to enter notification message
if(isset($_REQUEST['message']))
{
		
$message=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['message'])));
$messagetitle=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['messagetitle'])));

$servernotify = $conn->query("INSERT INTO `notification`(`messagetitle`,`message`,`messagedate`) VALUES('$messagetitle', '$message', '$dateenter')");

	if($servernotify){
		$messageEntered = array("entered"=>'entered');
		echo json_encode($messageEntered);
	}else{
		$messagefailed = array("fail"=>'failed');
		echo json_encode($messagefailed);
	}
	}
	
//script to get number of unread notification message and message id
if(isset($_REQUEST['unreaduserid']))
{
		
$unreaduserid=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['unreaduserid'])));

$unread = $conn->query("SELECT m.id, m.title, m.msg FROM messages m WHERE m.id NOT IN (SELECT rm.msgid FROM read_msg rm WHERE rm.userid = $unreaduserid)
");
$unreadnos = $unread->num_rows;
	if($unreadnos >= '1'){
	while($rowmsg = $unread->fetch_assoc()){
	$unreadmsgnos = array("unread"=> $unreadnos);
	$unreadmsg = array_merge($rowmsg,$unreadmsgnos);
	$unreadmsgs[] = $unreadmsg;
	$printmsg = json_encode($unreadmsgs);
	 } echo $printmsg;
	}else{
		$unreadmsg = array("unread"=> 'none');
		echo json_encode($unreadmsg);
	}
	}
	
//get message from admin
if(isset($_REQUEST['msgid']))
{
		
$msgid=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['msgid'])));
$rmuserid=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['rmuserid'])));

$msg = $conn->query("SELECT msg FROM messages WHERE id=$msgid ");
$msgnos = $msg->num_rows;
	if($msg){
		$opennedmsg = $conn->query("INSERT INTO `read_msg`(`msgid`,`userid`) VALUES('$msgid', '$rmuserid')");
	$msgresult = $msg->fetch_assoc();
	//$msgresult[] = $msgresult;
	$msgcontent = json_encode($msgresult);
	 echo $msgcontent;
	}
	}
	
//script to get unread message
if(isset($_REQUEST['notify']))
{
$userid=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['userid'])));

$unread = $conn->query("SELECT n.id, n.messages, rm.msgid FROM notification as n OUTTER JOIN readmsg as rm WHERE rm.userid = '$userid' AND n.id = rm.msgid");
$unreadnos = $unread->num_rows;
	if($unreadnos >= 1){
		$unreadmsg = array("unread"=> $unreadnos);
		echo json_encode($unreadmsg);
	}else{
		$unreadmsg = array("unread"=> 'none');
		echo json_encode($unreadmsg);
	}
	}
	
//script to enter notication to server
if(isset($_REQUEST['pushnotification']))
{
$userid=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['userid'])));
$msgid=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['msgid'])));

$readmsg = $conn->query("SELECT messages FROM notification WHERE id = '$msgid'");

	if($readmsg){
		//insert into read message table db
		$opennedmsg = $conn->query("INSERT INTO `readmsg`(`msg`,`userid`) VALUES('$msgid', '$userid')");
		
		$msg = array("msg"=> $readmsg);
		echo json_encode($msg);
		
	}else{
		$unreadmsg = array("unread"=> 'none');
		echo json_encode($unreadmsg);
	}
	}
	

//to set vote for station
if(isset($_REQUEST['rating_id'])){
$rating_id=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['rating_id'])));
$user_id=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['user_id'])));
$column=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['column'])));
$clicked_on=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['clicked_on'])));
$insert_date = date('Y-m-d h:i:s');

$rated = $conn->query("SELECT * FROM ratings WHERE user_id = '$user_id' AND station_id = $rating_id");
$ratedno = $rated->num_rows;
define('rating_column', $column);

if($ratedno != 0)
{
	$updaterating = $conn->query("UPDATE ratings SET ".rating_column." = $clicked_on, station_id = '$rating_id', user_id='$user_id' WHERE station_id=$rating_id AND user_id = $user_id ");
	echo $conn->error;
	if($updaterating){$rating = array("update"=>'updated');
	$rateupdated =  json_encode($rating);
	echo $rateupdated; }
	else 
	echo "error";
	
	
	
}
else
{

	$rating_insert = $conn->query("INSERT INTO `ratings`(`".rating_column."`,`user_id`,`station_id`, `record_date`) VALUES('$clicked_on', '$user_id', '$rating_id', '$insert_date') ");
	echo $conn->error;
	
	$rated = array("insert"=>'inserted');
	$rateupdated = json_encode($rated);
	echo $rateupdated; 
}
}
	
//script to search list of station
if(isset($_REQUEST['search']))
{

		
$searchitem= mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['search'])));
$searchuserid=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['userid'])));
$lon1=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['longi'])));
$lat1=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['latit'])));

//let the system search by price or address and nearness to the user of the application
$search = "SELECT s.reg_id, s.name, s.stationaddress, s.phone, s.longitude, s.latitude,f.fuelprice, f.paymethod, f.dieselprice,f.keroseneprice,f.status_update, f.updated 
FROM signup as s
INNER JOIN fuel_update as f
WHERE (s.name LIKE '%$searchitem%' OR s.stationaddress LIKE '%$searchitem%') AND f.station_id = s.reg_id";
$searchquery = $conn ->query($search);
echo $conn->error;
$searchresult = $searchquery->num_rows;
	if($searchresult >= '1'){


while ($searcharray=$searchquery->fetch_assoc()){
	
	
	$lat2= $result['longitude'];
	$lon2= $result['latitude'];
	$distance = distance($lat1, $lon1, $lat2, $lon2, $unit);
	$searchdistance=array('distance'=>$distance);
        $searchsuccess = array('searched'=>'successful');
	$searcharray = array_merge($searcharray,$searchdistance,$searchsuccess );
	$sarray[] = $searcharray;
	uksort($array, "cmp");
	$searchjson = json_encode($sarray);
	 };
	echo $searchjson;
	}else{
		$norecord[] = array("norecord"=>'<h2>No Record Found</h2>','searched'=>'error');
		$noresult = json_encode($norecord);
          echo $noresult;
		
	}
	}

$conn->close();
?>