<?php
header('Content-type: application/json');

require 'mail/smtp.php';

// script to connect to database
$host = "localhost";
$user = "siscomed_signup";      //$user = "root";
$password = "En!0s@b";            //$paswd=""
$db = "siscomed_signup";  //$db = "kidszone";
$conn = new mysqli($host, $user, $password, $db);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
date_default_timezone_get(Africa/Lagos);
$dateenter = date('d-m-Y h:i A');
function distance($lat1, $lon1, $lat2, $lon2, $unit) {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "M") {
    return ($miles * 1.609344);
  } else if ($unit == "N") {
      return ($miles * 0.8684);
    } else {
        return $miles;
      }
}
$lon1=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['longitude'])));
$lat1=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['latitude'])));
$unit = 'M';


//script to signup new user
if(isset($_REQUEST['phone']))
{
$fullname=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['name'])));
$email=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['email'])));
$phone=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['phone'])));
$password=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['password'])));
$passwd=password_hash($password, PASSWORD_DEFAULT);
$stationaddress=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['address'])));
$latitude =mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['lati'])));
$longitude =mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['long'])));
$category =mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['category'])));
$result = $conn->query("select * from signup where phone='$phone'");
$signupuser=$result->num_rows;
if($signupuser >= '1')
{
	$exist= array("exist"=>'exist');
	echo json_encode($exist);

}
else
{
$date=date("d-m-y h:i:s");
$q=$conn->query("insert into `signup` (`name`,`email`,`password`, `phone`, `stationaddress`, `latitude`, `longitude`, `category`) values 
('$fullname','$email','$passwd', '$phone', '$stationaddress', '$latitude', '$longitude', '$category')");
if($q)
{
$success= array("success"=>'success');
	echo json_encode($success);
}
else
{
$failed= array("failed"=>'failed');
	echo json_encode($failed);
}
}


}

//script to authenticate and login user
if(isset($_REQUEST['phone2']))
{
$phone =mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['phone2'])));
$password=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['password2'])));
//$passwd=password_hash($password, PASSWORD_DEFAULT);
$loginuser= $conn->query("select * from signup where phone='$phone'");
$login=$loginuser->num_rows;
//
$result=$loginuser->fetch_assoc();
$password_verify=$result['password'];
//echo $password_verify;
if($login=='1' && password_verify($password,$password_verify))
{

 	$array[] = $result;
	$json = json_encode($array);
	echo $json;

}
else
{
$false = array("false"=>'false');
	echo json_encode($false);

}

}

//script to list filling station close-by
if(isset($_REQUEST['latitude']))
{

$log= $conn->query("
SELECT s.reg_id, s.name, s.stationaddress, s.phone, s.longitude, s.latitude, f.fuelprice, f.dieselprice,f.keroseneprice, f.status_update, f.updated
FROM signup as s
INNER JOIN fuel_update as f
WHERE s.category='station' AND s.reg_id = f.station_id  
LIMIT 50
");
echo $conn->error;




	function cmp($a, $b)
	{return strcmp($a->distance, $b->distance);};
	

while ($result=$log->fetch_assoc()){
	
	
	$lat2= $result['longitude'];
	$lon2= $result['latitude'];
	$distance = distance($lat1, $lon1, $lat2, $lon2, $unit);
	$dist=array('distance'=>$distance);
	$result = array_merge($result,$dist);
	$array[] = $result;
	uksort($array, "cmp");
	$json = json_encode($array);
	 };
	 
	echo $json;
	
/*if($login!=0) 
{ 
	// code to display if any filling station is close by	
	
}
else
{
	// code to display if any filling station is not close by
}*/
}

//script to fuel update by filling station
if(isset($_REQUEST['fuelprice']))
{
$userid =mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['fstationid'])));
$fuelprice =mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['fuelprice'])));
$status=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['status'])));
$column=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['column'])));

$fuel = $conn->query("select station_id,status_update from fuel_update  where station_id = '$userid'");
$fetch = $fuel->fetch_array();
$status_update = $fetch['status_update'];
 
 //$status.$status_update;
	function status($status,$status_update,$column){
	//fuel update
	if($status == 2 && $column == 'fuelprice'){
	 $commodity = 'f';
	 
	 //check if status is currently selling. If selling, no need updating the status
	if(strpos($status_update,$commodity) !== false){
		$update = $status_update;
		return $update;
		
	}else {
		//add status
		$commodity = 'f';
		$update = $status_update.$commodity;
		return $update;
	}
	 
	}
	else if($status == 1 && $column == 'fuelprice'){
		//not selling
		$commodity = 'f';
		$update = str_replace($commodity,"",$status_update);
	 //check if status is currently selling. If selling, remove it
	 return $update;
	}
	
	//diesel update
	if($status == 2 && $column == 'dieselprice'){
	 $commodity = 'd';
	 
	 //check if status is currently selling. If selling, no need updating the status
	if(strpos($status_update,$commodity) !== false){
		$update = $status_update;
		return $update;
		
	}else {
		//add status
		$commodity = 'd';
		$update = $status_update.$commodity;
		return $update;
	}
	 
	}
	else if($status == 1 && $column == 'dieselprice'){
		//not selling
		$commodity = 'd';
		$update = str_replace($commodity,"",$status_update);
	 //check if status is currently selling. If selling, remove it
	 return $update;
	}
	
	//kerosene update
	if($status == 2 && $column == 'keroseneprice'){
	 $commodity = 'k';
	 
	 //check if status is currently selling. If selling, no need updating the status
	if(strpos($status_update,$commodity) !== false){
		$update = $status_update;
		return $update;
		
	}else {
		//add status
		$commodity = 'k';
		$update = $status_update.$commodity;
		return $update;
	}
	 
	}
	else if($status == 1 && $column == 'keroseneprice'){
		//not selling
		$commodity = 'k';
		$update = str_replace($commodity,"",$status_update);
	 //check if status is currently selling. If selling, remove it
	 return $update;
	}
	
	
	};
	$update = status($status,$status_update,$column) ;
 
define('table_column',$column);
	
$fuelprice_update = $fuel->num_rows;
if($fuelprice_update != 0)
{
	$updates = $conn->query("UPDATE fuel_update SET ".table_column." = $fuelprice, status_update = '$update', updated='$dateenter' WHERE station_id=$userid ");
	//echo "Error updating record: " . $conn->error;
	if($updates){$fuel = array("update"=>'updated');
	$fuelupdated =  json_encode($fuel);
	echo $fuelupdated; }
	else 
	echo "Error updating record: " . $conn->error;
	
	
	
}
else
{

	$updatexec = $conn->query("INSERT INTO `fuel_update`(`".table_column."`,`status_update`,`station_id`, `updated`) VALUES('$fuelprice', '$update', '$userid', '$dateenter') ");
	echo $conn->error;
	
	$fuel = array("update"=>'updated');
	$fuelnotupdated = json_encode($fuel);
	echo $fuelnotupdated; 
}

}

//script to update users information
if(isset($_REQUEST['setupdate']))
{
		
$settingstationid=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['sstationid'])));
$settingname=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['settingname'])));
$settingemail=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['settingemail'])));
$settingpassword=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['settingpassword'])));
$settingpasswd = password_hash($password, PASSWORD_DEFAULT);
$settingaddress=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['settingaddress'])));
$settingcategory=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['settingcategory'])));
$settinglong=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['settinglong'])));
$settinglati=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['settinglati'])));

if($settingcategory=='user'){
	$settinglong='';
	$settinglati='';
};

$updates = $conn->query("UPDATE signup SET name = '$settingname', email = '$settingemail', stationaddress = '$settingaddress', latitude = '$settinglati', longitude = '$settinglong', password = (CASE WHEN password IS NOT NULL THEN '.$settingpasswd.' END) WHERE reg_id='$settingstationid' ");
	//echo "Error updating record: " . $conn->error;
	if($updates){
	$updatesettings = array("update"=>'updated');
	echo json_encode($updatesettings);
	}else{
	$failedupdate = array("update"=>'failed');
	echo json_encode($failedupdate);
	}
	
}


//script to bookmark
if(isset($_REQUEST['bookmarkid']))
{
		
$bookmarkid=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['bookmarkid'])));
$userid=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['userid'])));
$bookstatus = 'booked';

$checkbookmark = "SELECT stationid FROM bookmarked WHERE stationid = '$bookmarkid' AND status = 'booked'";
$bookmarkquery = $conn ->query($checkbookmark);
$bookresult = $bookmarkquery->num_rows;
	if($bookresult=='1'){
		$deletebookmark = "DELETE FROM bookmarked WHERE stationid = '$bookmarkid'";
		$bookmarkquery = $conn ->query($deletebookmark);
		$deletedbookmark = array("komot"=>'komoted');
		echo json_encode($deletedbookmark);
	}else{
		$booking = $conn->query("INSERT INTO `bookmarked`(`userid`,`stationid`,`status`) VALUES('$userid', '$bookmarkid', '$bookstatus')");
		//echo "Error updating record: " . $conn->error;
		if($booking){
		$bookupdate = array("bookmarked"=>'booked');
		echo json_encode($bookupdate );
		}else{
		$failedbookup = array("fail"=>'failed');
		echo json_encode($failedbookup);
	}
	}
	}
	
if(isset($_REQUEST['favuserid']))
{
$userid=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['favuserid'])));
$favoritequery = $conn ->query(
		"SELECT s.reg_id, s.name, s.stationaddress, s.phone, s.longitude, s.latitude, b.userid, b.stationid, f.fuelprice, f.dieselprice, f.keroseneprice, f.status_update, f.updated
		FROM signup as s
		INNER JOIN bookmarked as b
		INNER JOIN fuel_update as f
		WHERE s.category='station' AND b.userid = '$userid'  AND s.reg_id = b.stationid AND s.reg_id = f.station_id 
		");
	//echo "Error fav record: " . $conn->error;
	$favcount = $favoritequery->num_rows;
	if($favcount>='1'){
	
	while ($favresult=$favoritequery->fetch_assoc()){
	$favlat2= $favresult['longitude'];
	$favlon2= $favresult['latitude'];
	$favdistance = distance($lat1, $lon1, $favlat2, $favlon2, $unit);
	$favdist=array('distance'=>$favdistance );
	$favresult= array_merge($favresult,$favdist);
	
	$favarray[] = $favresult;
	$favjson = json_encode($favarray);
	 };
	echo $favjson;
	}else{
		$nofav = array("nofav"=>'nofavorite');
		echo json_encode($nofav);
	}
	}
	
if(isset($_REQUEST['stationid']))
{
$stationid=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['stationid'])));
$stationquery = $conn ->query(
		"SELECT s.reg_id, s.name, s.stationaddress, s.phone, s.longitude, s.latitude, c.userid,c.commenter, c.stationid, c.comment,c.dateline, f.fuelprice,f.dieselprice,f.keroseneprice, f.status_update, f.updated
		FROM signup as s
		INNER JOIN userscomments as c
		INNER JOIN fuel_update as f
		WHERE c.stationid = '$stationid'  AND s.reg_id = c.stationid AND s.reg_id = f.station_id 
		");
		
	$stationcount = $stationquery->num_rows;
	if($stationcount >='1'){
	
	while ($stationresult=$stationquery->fetch_assoc()){
	$staionlat2= $stationresult['longitude'];
	$stationlon2= $stationresult['latitude'];
	$stationdistance = distance($lat1, $lon1, $staionlat2, $stationlon2, $unit);
	$stationdist=array('distance'=>$stationdistance);
	$stationresult= array_merge($stationresult,$stationdist);
	
	$stationarray[] = $stationresult;
	$stationjson = json_encode($stationarray);
	 };
	echo $stationjson;
	}else{
		$zerocomm = $conn ->query(
	"SELECT s.reg_id, s.name, s.stationaddress, s.phone, s.longitude, s.latitude, f.fuelprice,f.dieselprice,f.keroseneprice, f.status_update, f.updated
		FROM signup as s
		INNER JOIN fuel_update as f
		WHERE  f.station_id = '$stationid'  AND s.reg_id = f.station_id 
		");
		//echo $conn->error;
		$zecomm = $zerocomm->fetch_assoc();
		$zerolat2= $zecomm['longitude'];
		$zerolon2= $zecomm['latitude'];
		$zerostationdistance = distance($lat1, $lon1, $zerolat2, $zerolon2, $unit);
		$zerostationdist=array('distance'=>$zerostationdistance );
		$nocomment = array("nocomment"=>'No Comment');
		$zerostationresult= array_merge($zecomm,$zerostationdist,$nocomment);
		$stationarray[] = $zerostationresult;
		$zstationjson = json_encode($stationarray);
		echo $zstationjson;
		
	}
	}
	


if(isset($_REQUEST['comment']))
{
$comment=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['comment'])));
$cuserid=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['cuserid'])));
$cstationid=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['cstationid'])));

$commenter = $conn ->query(
		"SELECT name 
		FROM signup 
		WHERE reg_id = '$cuserid' 
		");
$commenter1 = $commenter->fetch_array();
$commenter2 = $commenter1['name'];

//$commenter3 = array("commenter"=>$commenter2);
	//json_encode($commenter3);
$number = $commenter->num_rows;

	if($number=='1'){
	$insertcomment =  $conn->query("INSERT INTO `userscomments`(`userid`, `stationid`, `commenter`, `comment`, `dateline`) VALUES('$cuserid', '$cstationid', '$commenter2', '$comment', '$dateenter')");
	$comment = htmlentities($comment);
	$time = date("Y-m-d H:i:s");
	 $done = array("done"=>'Done', "commenter"=>$commenter2,"comment"=>$comment, 'time'=>$time);
	 echo json_encode($done);
	}else{
	$nodone = array("notdone"=>'not done');
	echo json_encode($nodone);
	}
	}
	

if(isset($_REQUEST['passwordresetemail']))
{
$passwordresetemail=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['passwordresetemail'])));
//$passwordresetemail = filter_var($_POST["passwordresetemail"], FILTER_VALIDATE_EMAIL);
//$passworduserid=mysqli_real_escape_string($conn,htmlspecialchars(trim($_REQUEST['passworduserid'])));

$passwordreset = $conn ->query(
		"SELECT email,name
		FROM signup 
		WHERE email = '$passwordresetemail' 
		");
$passwordreset1 = $passwordreset->fetch_array();
$passwordname = $passwordreset1['name'];
$passwordemail = $passwordreset1['email'];

//$commenter3 = array("commenter"=>$commenter2);
	//json_encode($commenter3);
$number = $passwordreset->num_rows;
//echo $number;
	if($number=='1'){

		// Create the unique user password reset key
		$link= password_hash($passwordemail, PASSWORD_DEFAULT);

		// Create a url which we will direct them to reset their password
		$pwrurl = "www.siscomedia.com.ng/loginapp.php/reset_password.php?q=".$link;
		
		//recipient of email
		$mail->addAddress($passwordemail, $passwordname);
		
		// Mail them their key
		$body = "Dear ".ucfirst($passwordname).",<br><br>If this e-mail does not apply to you please ignore it. It appears that you have requested a password reset from our Fuel App  www.yoursitehere.com.ng<br><br>To reset your password, please click the link below. If you cannot click it, please paste it into your web browser\'s address bar.<br><br>" . $pwrurl . "<br><br>Thanks,<br>The Administrator";
		
		$mail->IsHTML(true);
		$mail->Body=$body;
		
		
		
		if (!$mail->send()) {
   	 $emailerr = array("mailerdeamon"=>'Mailer Deamon'. $mail->ErrorInfo);
	echo json_encode($emailerr);
	} else {
   	 $sent = array(
	 "done"=>'Done');
	 echo json_encode($sent);
	}
		
	}else{
	$emailsearch = array("notfound"=>'not found');
	echo json_encode($emailsearch);
	}
	}

    
// Was the password reset form submitted?
if (isset($_POST["submit"]))
{
	// Gather the post data
	$email = mysqli_real_escape_string($conn,htmlspecialchars(trim($_POST["resetemailform"])));
	$password = mysqli_real_escape_string($conn,htmlspecialchars(trim($_POST["resetpasswordform"])));
	$confirmpassword = mysqli_real_escape_string($conn,htmlspecialchars(trim($_POST["resetconfirmpasswordform"])));
	$hash = mysqli_real_escape_string($conn,htmlspecialchars(trim($_POST["q"])));

	// Generate the reset key
	$resetkey = password_hash($email, PASSWORD_DEFAULT);

	// Does the new reset key match the old one?
	if (password_verify($email, $hash))
	{
		if ($password == $confirmpassword)
		{
			//has and secure the password
			$password = password_hash($password, PASSWORD_DEFAULT);

			// Update the user's password
				$query = $conn->query("UPDATE signup SET password = '$password' WHERE email = '$email' ");
				
			echo "success";
		}
		else{
			echo "mismatch";}
	}
	else{
		echo "invalid";}
}


$conn->close();
?>