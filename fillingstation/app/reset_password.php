<!DOCTYPE html5>
<html>
<head>
<title>Password Reset </title>
 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
</head>
<body>
<h1> Filling Station GeoLocation Mobile App <br><small> Password Reset</small></h1>
<div id="alert"></div><br>

<input type="email" name="resetemailform" id="resetemailform" placeholder="Email" value="" />
<input type="text" name="resetpasswordform" id="resetpasswordform" placeholder="Password" value=""  />
<input type="text" name="resetconfirmpasswordform" id="resetconfirmpasswordform" placeholder="Confirm Password" value=""  />
<input type="hidden" id="q" value="<?php echo $_GET['q']; ?>" />

<button onclick="resetpaswd()">Change Password</button>


<script> 
                
  function validateEmail(email) {
                  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                  return re.test(email);
                }
                
                //Abort function
            function Abort()
                {
               throw new Error('This is not an error. This is just to abort javascript');
                }
  
     function resetpaswd(){
     var resetemailform = document.getElementById('resetemailform').value;
   var  resetpasswordform = $('#resetpasswordform').val();
   var  q = $('#q').val();
   var  resetconfirmpasswordform = $('#resetconfirmpasswordform').val();
   
   if(!validateEmail(resetemailform)){
   	 var error = 'err';
   	 $('#alert').html('Enter valid email address');
   }else
   $('#alert').empty();
   
    if(resetpasswordform===null || resetpasswordform===""){
            	 $('#alert').html('Password can not be empty');
                    var error = 'err';   
                }else
                $('#alert').empty();
                
    if(resetconfirmpasswordform===null || resetconfirmpasswordform===""){
            	 $('#alert').html('Confirm Password can not be empty');
                    var error = 'err';   
                }else
                $('#alert').empty();
   
   if( error ==='err')
                { Abort(); }
                
    //e.preventDefault();
   //alert(resetemailform+' '+resetpasswordform+' '+resetconfirmpasswordform);
 $.post("http://www.siscomedia.com.ng/loginapp/signup",
             { resetemailform: resetemailform,
               resetpasswordform: resetpasswordform,
	       resetconfirmpasswordform: resetconfirmpasswordform, 
               q: q
               },
             function(feedbck){
             console.log(feedbck);
	 			if(feedbck.success==="success"){
	 			$('#alert').html("Your password has been successfully reset.");}
	 			else if(feedbck.mismatch==="mismatch"){
	 			$('#alert').html("Your password's do not match.");}
	 			else{
	 			$('#alert').html("Your password reset key is invalid. Enter your registered email");
	 			}
	 			});
	};
	
</script>
</body>
</html>