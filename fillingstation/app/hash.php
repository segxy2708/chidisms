<?php
$passwd = 'charles';
$hash = password_hash($passwd, PASSWORD_DEFAULT);

if (password_verify($passwd, $hash)) {
    echo 'Password is valid!';
} else {
    echo 'Invalid password.';
}