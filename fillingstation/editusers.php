<?php
include_once('main.php');

if(check_login() != true) { exit; }
error_reporting(0);
if(isset($_GET['change_user_details']))
{
	$user_name = mysqli_real_escape_string($conn,trim($_POST['user_name']));
	$user_email = mysqli_real_escape_string($conn,$_POST['user_email']);
	$user_password = mysqli_real_escape_string($conn,$_POST['user_password']);
	$user_phone = mysqli_real_escape_string($conn,$_POST['user_phone']);
	$user_address = mysqli_real_escape_string($conn,$_POST['user_address']);
	$user_city = mysqli_real_escape_string($conn,$_POST['user_city']);
	$user_state = mysqli_real_escape_string($conn,$_POST['user_state']);
	$user_category = mysqli_real_escape_string($conn,$_POST['user_category']);
	$user_paymethod = mysqli_real_escape_string($conn,$_POST['user_paymethod']);
	echo change_user_details($user_name, $user_email, $user_password,$user_phone,$user_address,$user_city,$user_state,$user_category, $user_paymethod);
}

else
{
echo '<div class="box_div" id="cp_div"><div class="box_top_div"><a href="#">Start</a> &gt; User\'s detail</div><div class="box_body_div">';

	if($_SESSION['user_is_admin'] == '1')
	{

?>

<h3>Edit user's details</h3>

	<p class="smalltext_p">Password can be left blank to leave unchanged.</p>

	<form action="." id="user_details_form" autocomplete="off"><p>

	<div id="user_details_div"><div>
	<label for="user_name_input">Name:</label><br>
	<input type="text" id="user_name_input" value="<?php echo $_SESSION['person_name']; ?>"><br><br>
	<label for="user_email_input">Email:</label><br>
	<input type="email" id="user_email_input" autocapitalize="off" value="<?php echo $_SESSION['person_email']; ?>"><br><br>
	<label for="user_phone_input">Phone:</label><br>
	<input type="tel" id="user_phone_input" autocapitalize="off" value="<?php echo $_SESSION['person_phone']; ?>"><br><br>
	<label for="user_address_input">Address:</label><br>
	<input type="text" id="user_address_input" autocapitalize="off" value="<?php echo $_SESSION['stationaddress']; ?>"><br><br>
    <label for="user_city_input">LGA:</label><br>
	<input type="text" id="user_city_input" autocapitalize="off" value="<?php echo $_SESSION['stationcity']; ?>"><br><br>
    <label for="user_state_input">State:</label><br>
	<input type="text" id="user_state_input" autocapitalize="off" value="<?php echo $_SESSION['stationstate']; ?>"><br><br>
        <input type="hidden" id="country" value="Nigeria">
    <label for="user_category_input">Category:</label><br>
	<select id="user_category_input" >
        <option disabled selected><?php echo $_SESSION['category']; ?></option>
        <option>user</option>
        <option>station</option>
        </select>
         <label for="user_paymethod_input">Payment Method:</label><br>
	<select id="user_paymethod_input" >
        <option disabled selected><?php echo $_SESSION['paymethod']; ?></option>
        <option value="1">cash</option>
        <option value="2">cash and card</option>
        </select>
	</div><div>
	<label for="user_password_input">Password:</label><br>
	<input type="password" id="user_password_input"><br><br>
	<label for="user_password_confirm_input">Confirm password:</label><br>
	<input type="password" id="user_password_confirm_input">
	</div></div>

	<p><input type="submit" class="small_button blue_button" value="Update my details"></p>

	</p></form>

	<p id="user_details_message_p"></p>
	
	<?php

	}
}

?>