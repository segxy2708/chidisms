<?php

// Configuration

//function get_configuration($data)
//{
//    $conn;
//	$query = mysqli_query("SELECT * FROM " . global_mysqli_configuration_table)or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysqli_error()) . '</span>');
//	$configuration = mysqli_fetch_array($query);
//	return($configuration[$data]);
//}

// Password

date_default_timezone_get('Africa/Lagos');
$dateenter = date('d-m-Y');

function random_password()
{
	$password = rand('1001', '9999');
	return $password;
}

function encrypt_password($password)
{
	$password = crypt($password, '$1$' . global_salt);
	return($password);
}

function add_salt($password)
{
	$password = '$1$' . substr(global_salt, 0, -1) . '$' . $password;
	return($password);
}

function strip_salt($password)
{
	$password = str_replace('$1$' . substr(global_salt, 0, -1) . '$', '', $password);
	return($password);	
}

// String manipulation

function modify_email($email)
{
	$email = str_replace('@', '(at)', $email);
	$email = str_replace('.', '(dot)', $email);
	return($email);
}

// String validation

function validate_user_name($user_name)
{
	if(preg_match('/^[a-z æøåÆØÅ]{2,12}$/i', $user_name))
	{
		return(true);
	}
}

function validate_user_email($user_email)
{
	if(filter_var($user_email, FILTER_VALIDATE_EMAIL) && strlen($user_email) < 51)
	{
		return(true);
	}
}

function validate_user_password($user_password)
{
	if(strlen($user_password) > 3 && trim($user_password) != '')
	{
		return(true);
	}
}

function validate_price($price)
{
	if(is_numeric($price))
	{
		return(true);
	}
}

function validate_phone($phone)
{
	if(is_numeric($phone))
	{
		return(true);
	}
}

// User validation

function user_name_exists($user_name)
{
    global $conn;
	$query = mysqli_query($conn,"SELECT * FROM " . global_mysqli_users_table . " WHERE name='$user_name'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars($conn->error) . '</span>');

	if(mysqli_num_rows($query) > 0)
	{
		return(true);
	}
}

function user_email_exists($user_email)
{
    global $conn;
	$query = mysqli_query($conn,"SELECT * FROM " . global_mysqli_users_table . " WHERE email='$user_email'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars($conn->error) . '</span>');

	if(mysqli_num_rows($query) > 0)
	{
		return(true);
	}
}

// Login

function get_login_data($data)
{
	if($data == 'user_email' && isset($_COOKIE[global_cookie_prefix . '_user_email']))
	{
		return($_COOKIE[global_cookie_prefix . '_user_email']);
	}
	elseif($data == 'user_password' && isset($_COOKIE[global_cookie_prefix . '_user_password']))
	{
		return($_COOKIE[global_cookie_prefix . '_user_password']);
	}
}

function login($user_phone, $user_password, $user_remember)
{
    global $conn;
	//$user_password_encrypted = encrypt_password($user_password);
	//$user_password = add_salt($user_password);
	//$user_password = password_hash($user_password, PASSWORD_DEFAULT);
	
	$query = mysqli_query($conn,"SELECT * FROM " . global_mysqli_users_table . " WHERE phone='$user_phone' ")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars($conn->error) . '</span>');
		
		//check whether na admins
		//$getit = mysqli_fetch_array($query);
		
		$user = mysqli_fetch_array($query);
		$is_admin = $user['is_user_admin'];
		$hashed = $user['password'];
		if($is_admin == '1' && password_verify($user_password ,$hashed))
	//if(mysqli_num_rows($query) == 1)
	{
			

			$_SESSION['user_id'] = $user['reg_id'];
			$_SESSION['user_is_admin'] = $user['is_user_admin'];
			$_SESSION['user_email'] = $user['email'];
			$_SESSION['user_name'] = $user['name'];
			$_SESSION['user_phone'] = $user['phone'];
			$_SESSION['logged_in'] = '1';

			if($user_remember == '1')
			{
				$user_password = strip_salt($user['password']);

				setcookie(global_cookie_prefix . '_user_email', $user['email'], time() + 3600 * 24 * intval(global_remember_login_days));
				setcookie(global_cookie_prefix . '_user_password', $user_password, time() + 3600 * 24 * intval(global_remember_login_days));
			}

			return(1);
	}
	 else
		 {
			return $is_admin;
		 return 'You don\'t have authorization to view this dashboard';
		logout();
		
	 }
}

function check_login()
{
	if(isset($_SESSION['logged_in']))
	{
        global $conn;
		$user_id = $_SESSION['user_id'];
		$query = mysqli_query($conn,"SELECT * FROM " . global_mysqli_users_table . " WHERE reg_id='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars($conn->error) . '</span>');

		if(mysqli_num_rows($query) == 1)
		{
			return(true);
		}
		else
		{
			logout();
			echo '<script type="text/javascript">window.location.replace(\'.\');</script>';
		}
	}
	else
	{
		logout();
		echo '<script type="text/javascript">window.location.replace(\'.\');</script>';
	}
}

function logout()
{
	session_unset();
	setcookie(global_cookie_prefix . '_user_email', '', time() - 3600);
	setcookie(global_cookie_prefix . '_user_password', '', time() - 3600);
}

function create_user($user_name,$user_email,$user_password,$user_phone,$user_address,$user_city, $user_state,$user_category, $user_secret_code)
{
	global $conn;
    //Google Map geocode the address
//$json_a = json_decode($getlat, true);

$user_streetaddress = $user_address.' ';
$user_streetaddress .= $user_city.' ';
$user_streetaddress .= $user_state.' ';

$user_streetaddress= str_replace (" ", "+", $user_address);
   $details_url = "https://maps.googleapis.com/maps/api/geocode/json?address=" .$user_streetaddress. "&key=AIzaSyCsSe3OmkyY1Ts2pkJyXFWMAW7zYuqTaOA";

   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $details_url);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
   $geoloc = json_decode(curl_exec($ch), true);
 if($user_category=='station'){
		$user_latitude = $geoloc['results'][0]['geometry']['location']['lat']; // get lat for json
		$user_longitude = $geoloc['results'][0]['geometry']['location']['lng']; // get ing for json
 }else{
		$user_latitude = ''; // get lat for json
		$user_longitude = ''; // get ing for json
 }

    
	if(validate_user_name($user_name) != true)
	{
		return('<span class="error_span">Name must be <u>letters only</u> and be <u>2 to 12 letters long</u>. If your name is longer, use a short version of your name</span>');
	}
	elseif(validate_user_email($user_email) != true)
	{
		return('<span class="error_span">Email must be a valid email address and be no more than 50 characters long</span>');
	}
    elseif(validate_phone($user_phone) != true)
	{
		return('<span class="error_span">Phone number must be number.</span>');
	}
	elseif(validate_user_password($user_password) != true)
	{
		return('<span class="error_span">Password must be at least 4 characters</span>');
	}
	elseif(global_secret_code != '0' && $user_secret_code != global_secret_code)
	{
		return('<span class="error_span">Wrong secret code</span>');
	}
	elseif(user_name_exists($user_name) == true)
	{
		return('<span class="error_span">Name is already in use. If you have the same name as someone else, use another spelling that identifies you</span>');
	}
	elseif(user_email_exists($user_email) == true)
	{
		return('<span class="error_span">Email is already registered. <a href="#forgot_password">Forgot your password?</a></span>');
	}
	else
	{
		$query = mysqli_query($conn, "SELECT * FROM " . global_mysqli_users_table . "")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars($conn->error) . '</span>');

		if(mysqli_num_rows($query) == 0)
		{
			$user_is_admin = '1';
		}
		else
		{
			$user_is_admin = '0';
		}

		//$user_password = encrypt_password($user_password);
		$user_password = password_hash($user_password, PASSWORD_DEFAULT);

		mysqli_query($conn,"INSERT INTO " . global_mysqli_users_table . " (is_user_admin,name,email,password,phone,stationaddress,stationcity,stationstate,longitude, latitude,category) VALUES ($user_is_admin,'$user_name','$user_email','$user_password','$user_phone','$user_address','$user_city','$user_state','$user_longitude','$user_latitude','$user_category')")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysqli_error()) . '</span>');

		$user_password = strip_salt($user_password);

		//setcookie(global_cookie_prefix . '_user_email', $user_email, time() + 3600 * 24 * intval(global_remember_login_days));
		//setcookie(global_cookie_prefix . '_user_password', $user_password, time() + 3600 * 24 * intval(global_remember_login_days));

		return(1);
	}
}

function list_admin_users()
{
    global $conn;
	$query = mysqli_query($conn,"SELECT * FROM " . global_mysqli_users_table . " WHERE is_user_admin='1' ORDER BY name")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysqli_error()) . '</span>');

	if(mysqli_num_rows($query) < 1)
	{
		return('<span class="error_span">There are no admins</span>');
	}
	else
	{
		$return = '<table id="forgot_password_table"><tr><th>Name</th><th>Email</th></tr>';

		$i = 0;

		while($user = mysqli_fetch_array($query))
		{
			$i++;

			$return .= '<tr><td>' . $user['user_name'] . '</td><td><span id="email_span_' . $i . '"></span></td></tr><script type="text/javascript">$(\'#email_span_' . $i . '\').html(\'<a href="mailto:\'+$.base64.decode(\'' . base64_encode($user['user_email']) . '\')+\'">\'+$.base64.decode(\'' . base64_encode($user['user_email']) . '\')+\'</a>\');</script>';
		}

		$return .= '</table>';

		return($return);
	}
}

// Reservations

function highlight_day($day)
{
	$day = str_ireplace(global_day_name, '<span id="today_span">' . global_day_name . '</span>', $day);
	return $day;
}


// Admin control panel
function person_record($id)
{
	global $conn;
	$query = mysqli_query($conn,"SELECT * FROM " . global_mysqli_users_table . " WHERE reg_id=$id")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars($conn->error) . '</span>');
	$fetch = $query->fetch_assoc();
	$_SESSION['person_id'] = $fetch['reg_id'];
	$_SESSION['person_name'] = $fetch['name'];
	$_SESSION['person_email'] = $fetch['email'];
	$_SESSION['person_phone'] = $fetch['phone'];
	$_SESSION['stationaddress'] = $fetch['stationaddress'];
	$_SESSION['stationcity'] = $fetch['stationcity'];
	$_SESSION['stationstate'] = $fetch['stationstate'];
	$_SESSION['category'] = $fetch['category'];
	$_SESSION['person_lga'] = $fetch['phone'];

}


function list_stations()
{
	global $conn;
	$query = mysqli_query($conn,"SELECT * FROM " . global_mysqli_users_table . " WHERE category = 'station' ORDER BY name ")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars($conn->error) . '</span>');

	$users = '<table id="users_table"><tr><th>ID</th><th>Category</th><th>Name</th><th>Address</th><th>Phone</th><th>Commodities</th><th>Rating</th><th></th></tr>';

	while($user = mysqli_fetch_array($query))
	{
		$users .= '<tr id="user_tr_' . $user['reg_id'] . '"><td><label for="user_radio_' . $user['reg_id'] . '">' . $user['reg_id'] . '</label></td><td>' . $user['category'] . '</td><td><label for="user_radio_' . $user['reg_id'] . '">' . $user['name'] . '</label></td><td class="no-break-out" ><label for="user_radio_' . $user['reg_id'] . '">' . $user['stationaddress'].' '.$user['stationcity'] .' '.$user['stationstate'] . '</label></td><td>' . $user['phone'] .'</td><td >'.commodities($user['reg_id']).'</td><td>'.get_rating($user['reg_id']).'</td><td style="white-space: nowrap;"><input type="radio" name="user_radio" class="user_radio" id="user_radio_' . $user['reg_id'] . '" value="' . $user['reg_id'] . '"> <a href="#editusers" id="' . $user['reg_id'] . '" onclick="'.person_record($user['reg_id']).'"><img src="img/user-edit.png" class=" class="img-algn" /></a></td></tr>';
	}

	$users .= '</table>';

	return($users);
}

function list_individualusers()
{
	global $conn;
	$query = mysqli_query($conn,"SELECT * FROM " . global_mysqli_users_table . " WHERE category = 'user' ORDER BY name ")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars($conn->error) . '</span>');

	$users = '<table id="users_table"><tr><th>ID</th><th>Category</th><th>Name</th><th>Phone</th><th></th></tr>';

	while($user = mysqli_fetch_array($query))
	{
		$users .= '<tr id="user_tr_' . $user['reg_id'] . '"><td><label for="user_radio_' . $user['reg_id'] . '">' . $user['reg_id'] . '</label></td><td>' . $user['category'] . '</td><td><label for="user_radio_' . $user['reg_id'] . '">' . $user['name'] . '</label></td><td>' . $user['phone'] .'</td><td style="white-space: nowrap;"><input type="radio" name="user_radio" class="user_radio" id="user_radio_' . $user['reg_id'] . '" value="' . $user['reg_id'] . '"> <a href="#editusers" id="' . $user['reg_id'] . '" onclick="'.person_record($user['reg_id']).'"><img src="img/user-edit.png" class=" class="img-algn" /></a></td></tr>';
	}

	$users .= '</table>';

	return($users);
}



function get_rating($stationid){
	global $conn;
	$rating = $conn ->query(
		"SELECT (COUNT(CASE WHEN availability = 1 THEN 1 END ) * 1) a1, (COUNT(CASE WHEN cost = 1 THEN 1 END ) * 1) c1, (COUNT(CASE WHEN accurate_pump = 1 THEN 1 END) * 1) ap1, (COUNT(CASE WHEN customer_service = 1 THEN 1 END) * 1) cs1, (COUNT(CASE WHEN availability = 2 THEN 1 END ) * 2) a2, (COUNT(CASE WHEN cost = 2 THEN 1 END ) * 2) c2, (COUNT(CASE WHEN accurate_pump = 2 THEN 1 END) * 2) ap2, (COUNT(CASE WHEN customer_service = 2 THEN 1 END) * 2) cs2,(COUNT(CASE WHEN availability = 3 THEN 1 END ) * 3) a3, (COUNT(CASE WHEN cost = 3 THEN 1 END ) * 3) c3, (COUNT(CASE WHEN accurate_pump = 3 THEN 1 END) * 3) ap3, (COUNT(CASE WHEN customer_service = 3 THEN 1 END) * 3) cs3,(COUNT(CASE WHEN availability = 4 THEN 1 END ) * 4) a4, (COUNT(CASE WHEN cost = 4 THEN 1 END ) * 4) c4, (COUNT(CASE WHEN accurate_pump = 4 THEN 1 END) * 4) ap4, (COUNT(CASE WHEN customer_service = 4 THEN 1 END) * 4) cs4,(COUNT(CASE WHEN availability = 5 THEN 1 END ) * 5) a5, (COUNT(CASE WHEN cost = 5 THEN 1 END ) * 5) c5, (COUNT(CASE WHEN accurate_pump = 5 THEN 1 END) * 5) ap5, (COUNT(CASE WHEN customer_service = 5 THEN 1 END) * 5) cs5, COUNT(CASE WHEN availability = 1 THEN 1 END )  ta1, COUNT(CASE WHEN cost = 1 THEN 1 END )  tc1, COUNT(CASE WHEN accurate_pump = 1 THEN 1 END) tap1, COUNT(CASE WHEN customer_service = 1 THEN 1 END) tcs1, COUNT(CASE WHEN availability = 2 THEN 1 END ) ta2, COUNT(CASE WHEN cost = 2 THEN 1 END ) tc2, COUNT(CASE WHEN accurate_pump = 2 THEN 1 END) tap2, COUNT(CASE WHEN customer_service = 2 THEN 1 END) tcs2,COUNT(CASE WHEN availability = 3 THEN 1 END ) ta3, COUNT(CASE WHEN cost = 3 THEN 1 END ) tc3, COUNT(CASE WHEN accurate_pump = 3 THEN 1 END) tap3, COUNT(CASE WHEN customer_service = 3 THEN 1 END) tcs3,COUNT(CASE WHEN availability = 4 THEN 1 END ) ta4, COUNT(CASE WHEN cost = 4 THEN 1 END ) tc4, COUNT(CASE WHEN accurate_pump = 4 THEN 1 END) tap4, COUNT(CASE WHEN customer_service = 4 THEN 1 END) tcs4,COUNT(CASE WHEN availability = 5 THEN 1 END ) ta5, COUNT(CASE WHEN cost = 5 THEN 1 END ) tc5, COUNT(CASE WHEN accurate_pump = 5 THEN 1 END) tap5, COUNT(CASE WHEN customer_service = 5 THEN 1 END) tcs5
		FROM ratings WHERE station_id = $stationid ");
	$ratingresult=$rating->fetch_assoc();
	//echo $conn->error;
	$total_availability = $ratingresult['a1']+$ratingresult['a2']+$ratingresult['a3']+$ratingresult['a4']+$ratingresult['a5'];
        $total_cost = $ratingresult['c1']+$ratingresult['c2']+$ratingresult['c3']+$ratingresult['c4']+$ratingresult['c5'];
        $total_accurate_pump = $ratingresult['ap1']+$ratingresult['ap2']+$ratingresult['ap3']+$ratingresult['ap4']+$ratingresult['ap5'];
        $total_customer_service = $ratingresult['cs1']+$ratingresult['cs2']+$ratingresult['cs3']+$ratingresult['cs4']+$ratingresult['cs5'];

       //add the total number of voters
        $users_availability  = $ratingresult['ta1']+$ratingresult['ta2']+$ratingresult['ta3']+$ratingresult['ta4']+$ratingresult['ta5'];
        $users_cost = $ratingresult['tc1']+$ratingresult['tc2']+$ratingresult['tc3']+$ratingresult['tc4']+$ratingresult['tc5'];
        $users_accurate_pump = $ratingresult['tap1']+$ratingresult['tap2']+$ratingresult['tap3']+$ratingresult['tap4']+$ratingresult['tap5'];
        $users_customer_service = $ratingresult['tcs1']+$ratingresult['tcs2']+$ratingresult['tcs3']+$ratingresult['tcs4']+$ratingresult['tcs5'];

        $availability = round($total_availability / $users_availability);
         $cost = round($total_cost / $users_cost);
       $accurate_pump = round($total_accurate_pump / $users_accurate_pump);
        $customer_service = round($total_customer_service / $users_customer_service);
        $cummulative_rating = round(($cost + $accurate_pump + $availability + $customer_service) / 4, 1);
        $cummulative = round(($cost + $accurate_pump + $availability + $customer_service) / 4);
       
        return $cummulative;
}

//get commodities price set by filling stations
function commodities($stationid){
	global $conn;
	$stationquery = $conn ->query(
		"SELECT fuelprice,paymethod,dieselprice,keroseneprice, status_update, station_id
		FROM fuel_update WHERE station_id  = '$stationid'");
		
		$stationresult=$stationquery->fetch_assoc();
		$fuelprice = $stationresult['fuelprice'];
		$dieselprice = $stationresult['dieselprice'];
		$keroprice = $stationresult['keroseneprice'];
                $station_id = $stationresult['station_id'];
		$stat = $stationresult['status_update'];
		return fuelstatus($stat)."&#8358;<input type='text' value='".$fuelprice."' class='fuelprice' onclick='editcommodity(this)' id='".$stationid."' /><input type='checkbox' name='sellingfuel' id='".$stationid."' class='sellingfuel' value='f' onclick='sellingcommodity(this)'/>".dieselstatus($stat)."&#8358;<input type='text' value='".$dieselprice."' class='dieselprice' onclick='editcommodity(this)' id='".$stationid."' /><input type='checkbox' name='sellingdiesel' id='".$stationid."'class='sellingdiesel' value='d' onclick='sellingcommodity(this)' /> ".kerostatus($stat)."&#8358;<input type='text' value='".$keroprice."' class='keroseneprice' onclick='editcommodity(this)' id='".$stationid."' /><input type='checkbox' name='sellingkero' id='".$stationid."' class='sellingkero' value='k' onclick='sellingcommodity(this)' />";
		
		
}


//send new price to server via typing
function editcommodity($user_id,$price,$com_coln){
global $conn;
//check to see if price is in the system
//if not insert price
	$stationupdate = $conn->query("SELECT station_id FROM fuel_update WHERE station_id = '$user_id' ");
	$stationpresent = $stationupdate->num_rows;
	if($stationpresent >= '1'){
	
	$editcommodity = $conn ->query(
		"UPDATE fuel_update SET ".$com_coln." ='$price', updated='$dateenter' WHERE station_id='$user_id'");
		$conn->error;
		//return $stationpresent;
	return(1);
	}
	
//else update price
else{
	$insert_comm = $conn->query("INSERT INTO fuel_update(`station_id`, $com_coln, `updated` )VALUES('$user_id', '$price', '$dateenter')");
	$conn->error;
	//return 'updated';
	return(1);
	}
}

//selling commodity or not
function sellingcommodity($user_id,$commodity,$column,$status){
global $conn;

$commodityquery = $conn ->query(
		"SELECT status_update
		FROM fuel_update WHERE 
		station_id  = '$user_id'");

$commodityfetch = $commodityquery->fetch_array();
$status_update = $commodityfetch['status_update'];
		
function status($status,$status_update,$column, $commodity){
	//fuel update
	if($status == 2 && $column == 'sellingfuel'){
	 //$commodity = 'f';
	 
	 //check if status is currently selling. If selling, no need updating the status
	if(strpos($status_update,$commodity) !== false){
		$update = $status_update;
		return $update;
		
	}else {
		//add status
		//$commodity = 'f';
		$update = $status_update.$commodity;
		return $update;
	}
	 
	}
	else if($status == 1 && $column == 'sellingfuel'){
		//not selling
		//$commodity = 'f';
		$update = str_replace($commodity,"",$status_update);
	 //check if status is currently selling. If selling, remove it
	 return $update;
	}
	
	//diesel update
	if($status == 2 && $column == 'sellingdiesel'){
	 //$commodity = 'd';
	 
	 //check if status is currently selling. If selling, no need updating the status
	if(strpos($status_update,$commodity) !== false){
		$update = $status_update;
		return $update;
		
	}else {
		//add status
		//$commodity = 'd';
		$update = $status_update.$commodity;
		return $update;
	}
	 
	}
	else if($status == 1 && $column == 'sellingdiesel'){
		//not selling
		//$commodity = 'd';
		$update = str_replace($commodity,"",$status_update);
	 //check if status is currently selling. If selling, remove it
	 return $update;
	}
	
	//kerosene update
	if($status == 2 && $column == 'sellingkero'){
	 //$commodity = 'k';
	 
	 //check if status is currently selling. If selling, no need updating the status
	if(strpos($status_update,$commodity) !== false){
		$update = $status_update;
		return $update;
		
	}else {
		//add status
		//$commodity = 'k';
		$update = $status_update.$commodity;
		return $update;
	}
	 
	}
	else if($status == 1 && $column == 'sellingkero'){
		//not selling
		//$commodity = 'k';
		$update = str_replace($commodity,"",$status_update);
	 //check if status is currently selling. If selling, remove it
	 return $update;
	}
	
	
	};
	
	$commoditystatus = status($status,$status_update,$column,$commodity);
	
	
	$editcommodity = $conn ->query(
		"UPDATE fuel_update SET status_update ='$commoditystatus' WHERE station_id='$user_id'");
	if($editcommodity){
	return "1";}
	/*else{
	return "not saved";
	}*/
}


//commodity color code
function fuelstatus($stat){
           if (preg_match('/f/',$stat))
			return '<span class="green">f</span>';
		else 
			return '<span class="red">f</span>';
        }

function dieselstatus($stat){
           if (preg_match('/d/',$stat))
			return '<span class="green">d</span>';
		else 
			return '<span class="red">d</span>';
        }

function kerostatus($stat){
           if (preg_match('/k/',$stat))
			return '<span class="green">k</span>';
		else 
			return '<span class="red">k</span>';
        }
		
function reset_user_password($user_id)
{
	global $conn;
	//password = random_password();
	$password_encrypted = password_hash($user_password, PASSWORD_DEFAULT);
	$user_password = password_hash($user_password, PASSWORD_DEFAULT);

	mysqli_query("UPDATE " . global_mysqli_users_table . " SET password='$password_encrypted' WHERE reg_id='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars($conn->error) . '</span>');

	if($user_id == $_SESSION['user_id'])
	{
		return(0);
	}
	else
	{
		return('The password to the user with ID ' . $user_id . ' is now "' . $password . '". The user can now log in and change the password');
	}
}

function change_user_permissions($user_id)
{
	if($user_id == $_SESSION['user_id'])
	{
		return('<span class="error_span">Sorry, you can\'t use your superuser powers to remove them</span>');
	}
	else
	{
		mysqli_query("UPDATE " . global_mysqli_users_table . " SET user_is_admin = 1 - user_is_admin WHERE user_id='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysqli_error()) . '</span>');

		return(1);
	}
}

function delete_user_data($user_id, $data)
{
	global $conn;
	if($user_id == $_SESSION['user_id'] && $data != 'reservations')
	{
		return('<span class="error_span">Sorry, self-destructive behaviour is not accepted</span>');
	}
	else
	{
		
		if($data == 'reservations')
		{
			mysqli_query($conn, "DELETE FROM " . global_mysqli_fuelupdate_table . " WHERE id='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars($conn->error) . '</span>');
		}
		elseif($data == 'user')
		{
			mysqli_query($conn, "DELETE FROM " . global_mysqli_users_table . " WHERE reg_id='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars($conn->error) . '</span>');
			mysqli_query($conn,"DELETE FROM " . global_mysqli_fuelupdate_table . " WHERE id='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars($conn->error) . '</span>');
			
			
		}
		
		return(1);
	}
}

function delete_all($data)
{
	$user_id = $_SESSION['user_id'];

	if($data == 'reservations')
	{
		mysqli_query("DELETE FROM " . global_mysqli_fuelupdate_table . " WHERE reservation_user_id!='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysqli_error()) . '</span>');
	}
	elseif($data == 'users')
	{
		mysqli_query("DELETE FROM " . global_mysqli_users_table . " WHERE reg_id!='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysqli_error()) . '</span>');
		mysqli_query("DELETE FROM " . global_mysqli_fuelupdate_table . " WHERE id!='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysqli_error()) . '</span>');
	}
	elseif($data == 'everything')
	{
		mysqli_query("DELETE FROM " . global_mysqli_users_table . "")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysqli_error()) . '</span>');
		mysqli_query("DELETE FROM " . global_mysqli_fuelupdate_table . "")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysqli_error()) . '</span>');
	}

	return(1);
}

function save_system_configuration($price)
{
	if(validate_price($price) != true)
	{
		return('<span class="error_span">Price must be a number (use . and not , if you want to use decimals)</span>');
	}
	else
	{
		mysqli_query("UPDATE " . global_mysqli_configuration_table . " SET price='$price'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysqli_error()) . '</span>');
	}

	return(1);
}

// User control panel

function get_usage()
{
	$usage = '<table id="usage_table"><tr><th>Reservations</th><th>Cost</th><th>Current price per reservation</th></tr><tr><td>' . count_reservations($_SESSION['user_id']) . '</td><td>' . cost_reservations($_SESSION['user_id']) . ' ' . global_currency . '</td><td>' . global_price . ' ' . global_currency . '</td></tr></table>';
	return($usage);
}




function change_user_details($user_name, $user_email, $user_password,$user_phone,$user_address,$user_city,$user_state,$user_category, $user_paymethod)
{
	global $conn;
	$user_id = $_SESSION['person_id'];
	
	$user_streetaddress = $user_address.' ';
	$user_streetaddress .= $user_city.' ';
	$user_streetaddress .= $user_state.' ';

	$user_streetaddress= str_replace (" ", "+", $user_address);
	   $details_url = "https://maps.googleapis.com/maps/api/geocode/json?address=" .$user_streetaddress. "&key=AIzaSyCsSe3OmkyY1Ts2pkJyXFWMAW7zYuqTaOA";

   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $details_url);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
   $geoloc = json_decode(curl_exec($ch), true);
 if($user_category=='station'){
		$user_latitude = $geoloc['results'][0]['geometry']['location']['lat']; // get lat for json
		$user_longitude = $geoloc['results'][0]['geometry']['location']['lng']; // get ing for json
		
		mysqli_query("UPDATE " . global_mysqli_fuelupdate_table . " SET paymethod = '$user_paymethod' WHERE station_id='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysqli_error()) . '</span>');
 }else{
		$user_latitude = ''; // get lat for json
		$user_longitude = ''; // get ing for json
 }


	if(validate_user_name($user_name) != true)
	{
		return('<span class="error_span">Name must be <u>letters only</u> and be <u>2 to 12 letters long</u>. If your name is longer, use a short version of your name</span>');
	}
	if(validate_user_email($user_email) != true)
	{
		return('<span class="error_span">Email must be a valid email address and be no more than 50 characters long</span>');
	}
	elseif(validate_user_password($user_password) != true && !empty($user_password))
	{
		return('<span class="error_span">Password must be at least 4 characters</span>');
	}
	// elseif(user_name_exists($user_name) == true && $user_name != $_SESSION['user_name'])
	// {
		// return('<span class="error_span">Name is already in use. If you have the same name as someone else, use another spelling that identifies you</span>');
	// }
	// elseif(user_email_exists($user_email) == true && $user_email != $_SESSION['user_email'])
	// {
		// return('<span class="error_span">Email is already registered</span>');
	// }
	else
	{
		if(empty($user_password))
		{
			mysqli_query($conn,"UPDATE " . global_mysqli_users_table . " SET name='$user_name', email='$user_email', phone='$user_phone',stationaddress='$user_address',stationcity = '$user_city',stationstate = '$user_state', latitude = 'user_latitude', longitude = 'user_longitude',  category='$user_category' WHERE reg_id='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars($conn->error) . '</span>');
			
		}
		else
		{
			//$user_password = encrypt_password($user_password);
			$user_password = password_hash($user_password, PASSWORD_DEFAULT);
			mysqli_query($conn,"UPDATE " . global_mysqli_users_table . " SET name='$user_name', email='$user_email', user_password='$user_password',  phone='$user_phone',stationaddress='$user_address',stationcity = '$user_city',stationstate = '$user_state', latitude = '$user_latitude', longitude = '$user_longitude', category='$user_category' WHERE reg_id='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars($conn->error) . '</span>');
			
		}

		//$_SESSION['user_name'] = $user_name;
		//$_SESSION['user_email'] = $user_email;

		//$user_password = strip_salt($user_password);

		//setcookie(global_cookie_prefix . '_user_email', $user_email, time() + 3600 * 24 * intval(global_remember_login_days));
		//setcookie(global_cookie_prefix . '_user_password', $user_password, time() + 3600 * 24 * intval(global_remember_login_days));

		return(1);
	}
}

?>
