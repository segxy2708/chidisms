<?php
include_once('main.php');

if(check_login() != true) { exit; }

error_reporting(0);

if($_SESSION['user_is_admin'] == '1' && isset($_GET['list_users']))
{
	echo list_stations();
}
elseif($_SESSION['user_is_admin'] == '1' && isset($_GET['reset_user_password']))
{
	$user_id = mysqli_real_escape_string($conn,$_POST['user_id']);
	echo reset_user_password($user_id);
}
elseif($_SESSION['user_is_admin'] == '1' && isset($_GET['change_user_permissions']))
{
	$user_id = mysqli_real_escape_string($conn,$_POST['user_id']);
	echo change_user_permissions($user_id);
}
elseif($_SESSION['user_is_admin'] == '1' && isset($_GET['editcommodity']))
{
	$user_id = mysqli_real_escape_string($conn,$_POST['reg_id']);
	$price = mysqli_real_escape_string($conn,$_POST['price']);
	$com_coln = mysqli_real_escape_string($conn,$_POST['com_coln']);
	echo editcommodity($user_id,$price,$com_coln);
}
elseif($_SESSION['user_is_admin'] == '1' && isset($_GET['sellingcommodity']))
{
	$user_id = mysqli_real_escape_string($conn,$_POST['reg_id']);
	$commodity = mysqli_real_escape_string($conn,$_POST['commodity']);
	$column = mysqli_real_escape_string($conn,$_POST['column']);
	$status = mysqli_real_escape_string($conn,$_POST['status']);
	echo sellingcommodity($user_id,$commodity,$column,$status);
}
elseif($_SESSION['user_is_admin'] == '1' && isset($_GET['delete_user_data']))
{
	$user_id = mysqli_real_escape_string($conn,$_POST['user_id']);
	$data = $_POST['delete_data'];
	echo delete_user_data($user_id, $data);
}
elseif($_SESSION['user_is_admin'] == '1' && isset($_GET['delete_all']))
{
	$data = $_POST['delete_data'];
	echo delete_all($data);
}
elseif(isset($_GET['change_user_details']))
{
	$user_name = mysqli_real_escape_string($conn,trim($_POST['user_name']));
	$user_email = mysqli_real_escape_string($conn,$_POST['user_email']);
	$user_password = mysqli_real_escape_string($conn,$_POST['user_password']);
	$user_phone = mysqli_real_escape_string($conn,$_POST['user_phone']);
	$user_address = mysqli_real_escape_string($conn,$_POST['user_address']);
	$user_city = mysqli_real_escape_string($conn,$_POST['user_city']);
	$user_state = mysqli_real_escape_string($conn,$_POST['user_state']);
	$user_category = mysqli_real_escape_string($conn,$_POST['user_category']);
	echo change_user_details($user_name, $user_email, $user_password,$user_phone,$user_address,$user_city,$user_state,$user_category);
}
elseif(isset($_GET['create_user']))
{
	$user_name = mysqli_real_escape_string($conn,trim($_POST['user_name']));
	$user_email = mysqli_real_escape_string($conn,$_POST['user_email']);
	$user_password = mysqli_real_escape_string($conn,$_POST['user_password']);
	$user_phone = mysqli_real_escape_string($conn,$_POST['user_phone']);
	$user_address = mysqli_real_escape_string($conn,$_POST['user_address']);
	$user_city = mysqli_real_escape_string($conn,$_POST['user_city']);
	$user_state = mysqli_real_escape_string($conn,$_POST['user_state']);
	$user_category = mysqli_real_escape_string($conn,$_POST['user_category']);
	$user_secret_code = $_POST['user_secret_code'];
	echo create_user($user_name,$user_email,$user_password,$user_phone,$user_address,$user_city,$user_state,$user_category, $user_secret_code);
}
elseif(isset($_GET['new_user']))
{

?>

	<div class="box_div" id="login_div"><div class="box_top_div"><a href="#">Start</a> &gt; New user</div><div class="box_body_div">
	<div id="new_user_div"><div>

	<form action="." id="new_user_form"><p>

	<label for="user_name_input">Name:</label><br>
	<input type="text" id="user_name_input"><br><br>
	<label for="user_email_input">Email:</label><br>
	<input type="text" id="user_email_input" autocapitalize="off"><br><br>
    <label for="user_phone_input">Phone:</label><br>
	<input type="tel" id="user_phone_input"><br><br>
	<label for="user_password_input">Password:</label><br>
	<input type="password" id="user_password_input"><br><br>
	<label for="user_password_confirm_input">Confirm password:</label><br>
	<input type="password" id="user_password_confirm_input"><br><br>
    <label for="user_address">Address:</label><br>
	<input type="text" id="user_address"><br><br>
    <label for="user_city">LGA:</label><br>
	<input type="text" id="user_city"><br><br>
    <label for="user_state">State:</label><br>
	<input type="text" id="user_state"><br><br>
        <input type="hidden" id="country" value="Nigeria">
    <label for="user_category">Category:</label><br>
	<select id="user_category">
        <option>user</option>
        <option>station</option>
        </select><br><br>

<?php

	if(global_secret_code != '0')
	{
		echo '<label for="user_secret_code_input">Secret code: <sup><a href="." id="user_secret_code_a" tabindex="-1">What\'s this?</a></sup></label><br><input type="password" id="user_secret_code_input"><br><br>';
	}

?>

	<input type="submit" value="Create user">

	</p></form>

	</div><div>
	
	<p class="blue_p bold_p">Information:</p>
	<ul>
	<li>Ensure you enter the right address in order for the system to pick the location accurately</li>
	<li>Users created here can also log in from the mobile with same details</li>
	<li>Your password is encrypted and can't be read</li>
	</ul>

	<div id="user_secret_code_div">Secret code is used to only allow certain people to create a new user. Contact the webmaster by email at <span id="email_span"></span> to get the secret code.</div>

	<script type="text/javascript">$('#email_span').html('<a href="mailto:'+$.base64.decode('<?php echo base64_encode(global_webmaster_email); ?>')+'">'+$.base64.decode('<?php echo base64_encode(global_webmaster_email); ?>')+'</a>');</script>

	</div></div>

	<p id="new_user_message_p"></p>

	</div></div>

<?php

}
else
{
	echo '<div class="box_div" id="cp_div"><div class="box_top_div"><a href="#">Start</a> &gt; Station Control panel</div><div class="box_body_div">';

	if($_SESSION['user_is_admin'] == '1')
	{

?>
<h3><a href="#listindividualusers">Users administration</a></h3>
<a href="#new_user" class="float-right"> <img src="img/user-add.png" class="img-algn" />New user</a>
		<div id="users_div"><?php echo list_stations(); ?></div>

		<p class="center_p"><!--input type="button" class="small_button blue_button" id="reset_user_password_button" value="Reset password"--> <input type="button" class="small_button blue_button" id="change_user_permissions_button" value="Change permissions"> <input type="button" class="small_button" id="delete_user_reservations_button" value="Delete commodities"> <input type="button" class="small_button" id="delete_user_button" value="Delete user"></p>
		<p class="center_p" id="user_administration_message_p"></p>

		<hr>
		
<?php

	}
}
?>

