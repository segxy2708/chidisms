jQuery( document ).ready(function() {
    jQuery(".custom-box ul li:nth-child(2) img").css("display", "none");
	jQuery(".custom-box ul li:nth-child(3) img").css("display", "none");
	jQuery(".custom-box ul li:nth-child(4) img").css("display", "none");
	
		jQuery(".custom-box ul li:nth-child(1) .boxed-layout").css("border", "1px solid #aaaaaa");
		jQuery(".custom-box ul li:nth-child(2) .boxed-layout").css("border", "1px solid #eeeeee");
		jQuery(".custom-box ul li:nth-child(3) .boxed-layout").css("border", "1px solid #eeeeee");
		jQuery(".custom-box ul li:nth-child(4) .boxed-layout").css("border", "1px solid #eeeeee");
		
		jQuery(".custom-box ul li:nth-child(1) p").css("color", "#000000");
		jQuery(".custom-box ul li:nth-child(2) p").css("color", "#888888");
		jQuery(".custom-box ul li:nth-child(3) p").css("color", "#888888");
		jQuery(".custom-box ul li:nth-child(4) p").css("color", "#888888");	

		
jQuery('.custom-box ul li:nth-child(1)').hover(function(e) {
    e.stopPropagation();
	jQuery(".custom-box ul li:nth-child(1) img").css("display", "block");
    jQuery(".custom-box ul li:nth-child(2) img").css("display", "none");
	jQuery(".custom-box ul li:nth-child(3) img").css("display", "none");
	jQuery(".custom-box ul li:nth-child(4) img").css("display", "none");
	
	jQuery(".custom-box ul li:nth-child(1) p").css("color", "#000000");
	jQuery(".custom-box ul li:nth-child(2) p").css("color", "#888888");
	jQuery(".custom-box ul li:nth-child(3) p").css("color", "#888888");
	jQuery(".custom-box ul li:nth-child(4) p").css("color", "#888888");
		
	jQuery(".custom-box ul li:nth-child(1) .boxed-layout").css("border", "1px solid #aaaaaa");
	jQuery(".custom-box ul li:nth-child(2) .boxed-layout").css("border", "1px solid #eeeeee");
	jQuery(".custom-box ul li:nth-child(3) .boxed-layout").css("border", "1px solid #eeeeee");
	jQuery(".custom-box ul li:nth-child(4) .boxed-layout").css("border", "1px solid #eeeeee");
	});
    
jQuery('.custom-box ul li:nth-child(2)').hover(function(e) {
    e.stopPropagation();
	jQuery(".custom-box ul li:nth-child(1) img").css("display", "none");
    jQuery(".custom-box ul li:nth-child(2) img").css("display", "block");
	jQuery(".custom-box ul li:nth-child(3) img").css("display", "none");
	jQuery(".custom-box ul li:nth-child(4) img").css("display", "none");
	
	jQuery(".custom-box ul li:nth-child(1) p").css("color", "#888888");
	jQuery(".custom-box ul li:nth-child(2) p").css("color", "#000000");
	jQuery(".custom-box ul li:nth-child(3) p").css("color", "#888888");
	jQuery(".custom-box ul li:nth-child(4) p").css("color", "#888888");
		
	jQuery(".custom-box ul li:nth-child(1) .boxed-layout").css("border", "1px solid #eeeeee");
	jQuery(".custom-box ul li:nth-child(2) .boxed-layout").css("border", "1px solid #aaaaaa");
	jQuery(".custom-box ul li:nth-child(3) .boxed-layout").css("border", "1px solid #eeeeee");
	jQuery(".custom-box ul li:nth-child(4) .boxed-layout").css("border", "1px solid #eeeeee");
    });  
   
jQuery('.custom-box ul li:nth-child(3)').hover(function(e) {
    e.stopPropagation();
	jQuery(".custom-box ul li:nth-child(1) img").css("display", "none");
    jQuery(".custom-box ul li:nth-child(2) img").css("display", "none");
	jQuery(".custom-box ul li:nth-child(3) img").css("display", "block");
	jQuery(".custom-box ul li:nth-child(4) img").css("display", "none");
	
	jQuery(".custom-box ul li:nth-child(1) p").css("color", "#888888");
	jQuery(".custom-box ul li:nth-child(2) p").css("color", "#888888");
	jQuery(".custom-box ul li:nth-child(3) p").css("color", "#000000");
	jQuery(".custom-box ul li:nth-child(4) p").css("color", "#888888");
		
	jQuery(".custom-box ul li:nth-child(1) .boxed-layout").css("border", "1px solid #eeeeee");
	jQuery(".custom-box ul li:nth-child(2) .boxed-layout").css("border", "1px solid #eeeeee");
	jQuery(".custom-box ul li:nth-child(3) .boxed-layout").css("border", "1px solid #aaaaaa");
	jQuery(".custom-box ul li:nth-child(4) .boxed-layout").css("border", "1px solid #eeeeee");		
    });  
  		
jQuery('.custom-box ul li:nth-child(4)').hover(function(e) {
    e.stopPropagation(); 
	jQuery(".custom-box ul li:nth-child(1) img").css("display", "none");
    jQuery(".custom-box ul li:nth-child(2) img").css("display", "none");
	jQuery(".custom-box ul li:nth-child(3) img").css("display", "none");
	jQuery(".custom-box ul li:nth-child(4) img").css("display", "block");
	
	jQuery(".custom-box ul li:nth-child(1) p").css("color", "#888888");
	jQuery(".custom-box ul li:nth-child(2) p").css("color", "#888888");
	jQuery(".custom-box ul li:nth-child(3) p").css("color", "#888888");
	jQuery(".custom-box ul li:nth-child(4) p").css("color", "#000000");
		
	jQuery(".custom-box ul li:nth-child(1) .boxed-layout").css("border", "1px solid #eeeeee");
	jQuery(".custom-box ul li:nth-child(2) .boxed-layout").css("border", "1px solid #eeeeee");
	jQuery(".custom-box ul li:nth-child(3) .boxed-layout").css("border", "1px solid #eeeeee");
	jQuery(".custom-box ul li:nth-child(4) .boxed-layout").css("border", "1px solid #aaaaaa");
	});    
});  

