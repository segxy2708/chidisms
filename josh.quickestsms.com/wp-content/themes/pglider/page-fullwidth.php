<?php
/**
 * The template for displaying all pages.
 * Template Name: Full Width Page
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package pGlider
 */

get_header(); 
$quadbox_toggle = get_theme_mod('purelyglider_quadbox_toggle');
$quadbox_types = get_theme_mod('purelyglider_quadbox_types');
$category_toggle = get_theme_mod('purelyglider_category_toggle');
$category_types = get_theme_mod('purelyglider_category_types');
$category_amount = get_theme_mod('purelyglider_category_amount');
$category_desc = get_theme_mod('purelyglider_category_descr');
?>

	<div id="primary" class="content-area pure-full-width">
		<main id="main" class="site-main" role="main">

		
		<?php if ($quadbox_toggle == 'show') { ?>
		<div class="custom-box-container">
			<div id="custom-box" class="col grid_12_of_12">
				<div class="custom-box">
				<?php 
					$args = array(
					'post_type' => 'product',
					'posts_per_page' => '4',
					'ignore_sticky_posts' => 'true',
					'meta_query'     => array()
					);
				
					$args['meta_query']   = array_filter( $args['meta_query'] );
					switch ( $quadbox_types ) {
			
					case 'featured' :
					$args['meta_query'][] = array(
					'key'   => '_featured',
					'value' => 'yes'
					);
					break;
					case 'onsale' :
					$product_ids_on_sale    = wc_get_product_ids_on_sale();
					$product_ids_on_sale[]  = 0;
					$args['post__in'] = $product_ids_on_sale;
					break;
					}	
				?>
				<?php $the_query = new WP_Query( $args ); ?>
				<?php if ( $the_query->have_posts() ) : ?>
				<ul class="product_list_widget">
				
				<!-- the loop -->
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<li>
						<a href="<?php echo esc_url( get_permalink( $product->id ) ); ?>" title="<?php echo esc_attr( $product->get_title() ); ?>">
							<?php echo $product->get_image(array(550,550)); ?>
						</a>
						<div class="boxed-layout">
							<a href="<?php echo esc_url( get_permalink( $product->id ) ); ?>" title="<?php echo esc_attr( $product->get_title() ); ?>">
								<span class="product-title"><?php echo $product->get_title(); ?></span>
							</a>
							<div class="price-stuff">
								<?php if ( ! empty( $show_rating ) ) echo $product->get_rating_html(); ?>
								<?php echo $product->get_price_html(); ?>
							</div>
							<?php
								if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
									global $post;
								if ( ! $post->post_excerpt ) return;
							?>
						<?php if ( is_front_page() ) { ?>	
						<div id="description">
								<?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ) ?>
						</div>
						<?php } ?>
						</div>
					</li>
				<?php endwhile; ?>
				<!-- end of the loop -->
				</ul>
				</div>
					<?php wp_reset_postdata(); ?>
					<?php else : ?>
						<p><?php _e( 'Sorry, no posts matched your criteria.', 'emporium' ); ?></p>
					<?php endif; ?>
			</div><!-- #custom-box -->
		</div><!-- .custom-box-container --> 
		<?php } ?>
		

	<?php if ($category_toggle == 'show') { ?>
		<div class="product-category-list">
			<?php 
				$args = array(
					'post_type' => 'product',
					'posts_per_page' => $category_amount,
					'ignore_sticky_posts' => 'true',
					'product_cat' => $category_types,
					);
				$the_query = new WP_Query( $args );
				if ( $the_query->have_posts() ) : 
			?>
				
			<ul class="category-list">
				
				<!-- the loop -->
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<li>
						<a href="<?php echo esc_url( get_permalink( $product->id ) ); ?>" title="<?php echo esc_attr( $product->get_title() ); ?>">
							<?php echo $product->get_image(array(200,200)); ?>
						</a>
						<div class="product-content">
							<a href="<?php echo esc_url( get_permalink( $product->id ) ); ?>" title="<?php echo esc_attr( $product->get_title() ); ?>">
								<span class="product-title"><?php echo $product->get_title(); ?></span>
							</a>
							<div class="product-price">
								<?php if ( ! empty( $show_rating ) ) echo $product->get_rating_html(); ?>
								<?php echo $product->get_price_html(); ?>
							</div>
							<?php
								if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
									global $post;
								if ( ! $post->post_excerpt ) return;
							?>
							<?php if ($category_desc == 'show') { ?>
							<div class="product-description">
							<?php $excerpt = apply_filters( 'woocommerce_short_description', $post->post_excerpt );
								echo substr($excerpt,0, 140);
								echo "..";
							?>
							</div>
							<?php } ?>
						</div>
					</li>
				<?php endwhile; ?>
				<!-- end of the loop -->
			</ul>
		</div>
		<?php wp_reset_postdata(); ?>
		<?php else : ?>
			<p><?php _e( 'Sorry, no posts matched your criteria.', 'emporium' ); ?></p>
		<?php endif; ?>
		
	<?php } ?>		
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				?>

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php if (!is_front_page()) :
			get_sidebar();
		endif;
?>

<?php get_footer(); ?>
