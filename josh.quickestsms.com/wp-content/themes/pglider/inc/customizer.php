<?php
/**
 * pGlider Theme Customizer
 *
 * @package pGlider
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function purelyglider_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
	
	// Adding in new section to handle frontpage layout css
	$wp_customize->add_section( 'purelyglider_homepage' , array(
    'title'      => __( 'pGlider Options', 'purelyglider' ),
    'priority'   => 30,
	) );
	
	//  quadbox customization

	//Settings for quadbox widget
	$wp_customize->add_setting( 'purelyglider_quadbox_toggle', array(
    'default'     => 'hide',
	'transport' => 'refresh',
	'sanitize_callback' => 'sanitize_text_field',
	));		
	$wp_customize->add_setting( 'purelyglider_quadbox_types' , array(
    'default'     => 'recent',
	'sanitize_callback' => 'sanitize_text_field',
	'transport'   => 'refresh',
	) );
	// quadbox toggle
	$wp_customize->add_control(	'purelyglider_quadbox_toggle', 	array(
		'label'    => __( 'Show or Hide Slider', 'purelyglider' ),
		'section'  => 'purelyglider_homepage',
		'settings' => 'purelyglider_quadbox_toggle',
		'type'     => 'select',
		'choices'  => array(
		'show'  => 'Show glider',
		'hide' => 'Hide glider',
		),
	));		
	
	// Controls for the 6 different shortcodes in page template: page-home-shop.php
	$wp_customize->add_control(	'purelyglider_quadbox_types', 	array(
		'label'    => __( 'Type of products to show in slider', 'purelyglider' ),
		'section'  => 'purelyglider_homepage',
		'settings' => 'purelyglider_quadbox_types',
		'type'     => 'select',
		'choices'  => array(
		'featured'  => 'Featured products',
		'recent' => 'Recent Products',
		'onsale' => 'On Sale',
		),
	));
	

	//Settings for category widget
	$wp_customize->add_setting( 'purelyglider_category_toggle', array(
    'default'     => 'hide',
	'transport' => 'refresh',
	'sanitize_callback' => 'sanitize_text_field',
	));		
	$wp_customize->add_setting( 'purelyglider_category_types', array(
	'sanitize_callback' => 'sanitize_text_field',
	));		
	$wp_customize->add_setting( 'purelyglider_category_amount', array(
	'sanitize_callback' => 'sanitize_text_field',
	'default' => '5',
	));		
	$wp_customize->add_setting( 'purelyglider_category_descr', array(
    'default'     => 'hide',
	'transport' => 'refresh',
	'sanitize_callback' => 'sanitize_text_field',
	));	
	// category toggle
	$wp_customize->add_control(	'purelyglider_category_toggle', 	array(
		'label'    => __( 'Show or Hide Categories', 'purelyglider' ),
		'section'  => 'purelyglider_homepage',
		'settings' => 'purelyglider_category_toggle',
		'type'     => 'select',
		'choices'  => array(
		'show'  => 'Show glider',
		'hide' => 'Hide glider',
		),
	));		
	// category names
	$wp_customize->add_control(	'purelyglider_category_types', 	array(
		'label'    => __( 'Category Names, comma separated. Empty for all', 'purelyglider' ),
		'section'  => 'purelyglider_homepage',
		'settings' => 'purelyglider_category_types',
		'type'     => 'text',
	));		
		
	// category amount
	$wp_customize->add_control(	'purelyglider_category_amount', 	array(
		'label'    => __( 'Number of products to show under category', 'purelyglider' ),
		'section'  => 'purelyglider_homepage',
		'settings' => 'purelyglider_category_amount',
		'type'     => 'text',
	));		
	// description in category?
	$wp_customize->add_control(	'purelyglider_category_descr', 	array(
		'label'    => __( 'Show or Hide Description', 'purelyglider' ),
		'section'  => 'purelyglider_homepage',
		'settings' => 'purelyglider_category_descr',
		'type'     => 'select',
		'choices'  => array(
		'show'  => 'Show description',
		'hide' => 'Hide description',
		),
	));		
}
add_action( 'customize_register', 'purelyglider_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function purelyglider_customize_preview_js() {
	wp_enqueue_script( 'purelyglider_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'purelyglider_customize_preview_js' );
