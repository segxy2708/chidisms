<?php
/**
 * Theme upsell page 
 *
 *
 * @package purelyglider
 */
?>

<?php
function purelyglider_upsell_page() {
	add_theme_page( __('More Themes', 'purelyglider'), __('More Themes', 'purelyglider'), 'administrator', 'purelythemes', 'purelyglider_upsell_view' );

}
add_action( 'admin_menu', 'purelyglider_upsell_page', 20 );

function purelyglider_upsell_view() {
	// Define Variables
	$directory_uri = get_template_directory_uri();
	?>
<div class="wrap">
		<h2><?php _e('More themes by the author', 'purelyglider'); ?></h2>
		<p><?php _e('Thanks for downloading one of our themes, we really appreciate it!', 'purelyglider'); ?></p> 
		<hr />
	<div class="theme-browser">
		<div class="themes">
			<div class="theme">
				<div class="theme-screenshot">
					<a href="<?php echo esc_url( __('http://www.purelythemes.com', 'pGlider'));?>" target="_blank"><img src="<?php echo $directory_uri; ?>/inc/img/sylvia.jpg" alt=""></a>
				</div>
				<h3 class="theme-name"><?php _e('Sylvia', 'purelyglider'); ?></h3>
				<div class="theme-actions">
					<a href="<?php echo esc_url( __('http://www.purelythemes.com', 'pGlider'));?>" class="button button-primary" target="_blank"><?php _e('Read More', 'purelyglider'); ?></a>
				</div>
			</div>	
			<div class="theme">
				<div class="theme-screenshot">
					<a href="<?php echo esc_url( __('http://www.purelythemes.com', 'pGlider'));?>" target="_blank"><img src="<?php echo $directory_uri; ?>/inc/img/skylar.jpg" alt=""></a>
				</div>
				<h3 class="theme-name"><?php _e('Skylar', 'purelyglider'); ?></h3>
				<div class="theme-actions">
					<a href="<?php echo esc_url( __('http://www.purelythemes.com', 'pGlider'));?>" class="button button-primary" target="_blank"><?php _e('See Demo', 'purelyglider'); ?></a>
				</div>
			</div>	
			<div class="theme">
				<div class="theme-screenshot">
					<a href="<?php echo esc_url( __('http://www.purelythemes.com', 'pGlider'));?>" target="_blank"><img src="<?php echo $directory_uri; ?>/inc/img/emporium.jpg" alt=""></a>
				</div>
				<h3 class="theme-name"><?php _e('Emporium', 'purelyglider'); ?></h3>
				<div class="theme-actions">
					<a href="<?php echo esc_url( __('http://www.purelythemes.com', 'pGlider'));?>" class="button button-primary" target="_blank"><?php _e('See Demo', 'purelyglider'); ?></a>
				</div>
			</div>	
			<div class="theme">
				<div class="theme-screenshot">
					<a href="<?php echo esc_url( __('http://www.purelythemes.com', 'pGlider'));?>" target="_blank"><img src="<?php echo $directory_uri; ?>/inc/img/stella.jpg" alt=""></a>
				</div>
				<h3 class="theme-name"><?php _e('stella', 'purelyglider'); ?></h3>
				<div class="theme-actions"><a class="button button-primary" target="_blank" href="<?php echo esc_url( __('http://www.purelythemes.com', 'pGlider'));?>"><?php _e('See Demo', 'purelyglider'); ?></a></div>
			</div>	
			<div class="theme">
				<div class="theme-screenshot">
					<a href="<?php echo esc_url( __('http://www.purelythemes.com', 'pGlider'));?>" target="_blank"><img src="<?php echo $directory_uri; ?>/inc/img/shopping.jpg" alt=""></a>
				</div>
				<h3 class="theme-name"><?php _e('purelyglider', 'purelyglider'); ?></h3>
				<div class="theme-actions">
					<a class="button button-primary" target="_blank" href="<?php echo esc_url( __('http://www.purelythemes.com', 'pGlider'));?>"><?php _e('See Demo', 'purelyglider'); ?></a>
				</div>
			</div>	
			<div class="theme">
				<div class="theme-screenshot">
					<a href="<?php echo esc_url( __('http://www.purelythemes.com', 'pGlider'));?>" target="_blank"><img src="<?php echo $directory_uri; ?>/inc/img/blue.jpg" alt=""></a>
				</div>
				<h3 class="theme-name"><?php _e('PurelyBlue', 'purelyglider'); ?></h3>
				<div class="theme-actions">
					<a class="button button-primary" target="_blank" href="<?php echo esc_url( __('http://www.purelythemes.com', 'pGlider'));?>"><?php _e('See Demo', 'purelyglider'); ?></a>
				</div>
			</div>	
			<div class="theme">
				<div class="theme-screenshot">
					<a href="<?php echo esc_url( __('http://www.purelythemes.com', 'pGlider'));?>" target="_blank"><img src="<?php echo $directory_uri; ?>/inc/img/glider.jpg" alt=""></a>
				</div>
				<h3 class="theme-name"><?php _e('purelyglider', 'purelyglider'); ?></h3>
				<div class="theme-actions">
					<a class="button button-primary" target="_blank" href="<?php echo esc_url( __('http://www.purelythemes.com', 'pGlider'));?>"><?php _e('See Demo', 'purelyglider'); ?></a>
				</div>
			</div>	
			<div class="theme">
				<div class="theme-screenshot">
					<a href="<?php echo esc_url( __('http://www.purelythemes.com', 'pGlider'));?>" target="_blank"><img src="<?php echo $directory_uri; ?>/inc/img/selma.jpg" alt=""></a>
				</div>
				<h3 class="theme-name"><?php _e('Selma', 'purelyglider'); ?></h3>
				<div class="theme-actions">
					<a class="button button-primary" target="_blank" href="<?php echo esc_url( __('http://www.purelythemes.com', 'pGlider'));?>"><?php _e('See Demo', 'purelyglider'); ?></a>
				</div>
			</div>	
		</div>
	</div>	
</div>
	<?php // Close purelyglider_upsell_view()
} 
	?>