<?php global $product; ?>
<li>
	<a href="<?php echo esc_url( get_permalink( $product->id ) ); ?>" title="<?php echo esc_attr( $product->get_title() ); ?>">
		<?php echo $product->get_image(array(150,150)); ?>
	</a>
	<div class="boxed-layout">
	<a href="<?php echo esc_url( get_permalink( $product->id ) ); ?>" title="<?php echo esc_attr( $product->get_title() ); ?>">
		<span class="product-title"><?php echo $product->get_title(); ?></span>
	</a>
	<div class="price-stuff">
	<?php if ( ! empty( $show_rating ) ) echo $product->get_rating_html(); ?>
	<?php echo $product->get_price_html(); ?>
	</div>
	<?php
	if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

	global $post;

	if ( ! $post->post_excerpt ) return;
	?>
	<a href="<?php echo esc_url( get_permalink( $product->id ) ); ?>" title="<?php echo esc_attr( $product->get_title() ); ?>">
	<div id="description">
		<?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ) ?>
	</div>
	</a>
	
	</div>
</li>