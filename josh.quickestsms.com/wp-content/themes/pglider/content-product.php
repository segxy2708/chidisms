<?php
/**
 * The template part for displaying WooCommerce Products
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package pGlider
 */
?>
<div class="custom-box-container">
			<div id="custom-box" class="col grid_12_of_12">
				<div class="custom-box">
				<?php 
					$args = array(
					'post_type' => 'product',
					'posts_per_page' => '4',
					'ignore_sticky_posts' => 'true',
					'meta_query'     => array()
					);
				
					$args['meta_query']   = array_filter( $args['meta_query'] );
					$quadbox_types = isset($instance['quadbox_types']) ? $instance['quadbox_types'] : '';
					switch ( $quadbox_types ) {
			
					case 'featured' :
					$args['meta_query'][] = array(
					'key'   => '_featured',
					'value' => 'yes'
					);
					break;
					case 'onsale' :
					$product_ids_on_sale    = wc_get_product_ids_on_sale();
					$product_ids_on_sale[]  = 0;
					$args['post__in'] = $product_ids_on_sale;
					break;
					}	
				?>
				<?php $the_query = new WP_Query( $args ); ?>
				<?php if ( $the_query->have_posts() ) : ?>
				<ul class="product_list_widget">
				
				<!-- the loop -->
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<li>
						<a href="<?php echo esc_url( get_permalink( $product->id ) ); ?>" title="<?php echo esc_attr( $product->get_title() ); ?>">
							<?php echo $product->get_image(array(550,550)); ?>
						</a>
						<div class="boxed-layout">
							<a href="<?php echo esc_url( get_permalink( $product->id ) ); ?>" title="<?php echo esc_attr( $product->get_title() ); ?>">
								<span class="product-title"><?php echo $product->get_title(); ?></span>
							</a>
							<div class="price-stuff">
								<?php if ( ! empty( $show_rating ) ) echo $product->get_rating_html(); ?>
								<?php echo $product->get_price_html(); ?>
							</div>
							<?php
								if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
									global $post;
								if ( ! $post->post_excerpt ) return;
							?>
						<?php if ( is_front_page() ) { ?>	
						<div id="description">
								<?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ) ?>
						</div>
						<?php } ?>
						</div>
					</li>
				<?php endwhile; ?>
				<!-- end of the loop -->
				</ul>
				</div>
					<?php wp_reset_postdata(); ?>
					<?php else : ?>
						<p><?php _e( 'Sorry, no posts matched your criteria.', 'emporium' ); ?></p>
					<?php endif; ?>
			</div><!-- #custom-box -->
		</div><!-- .custom-box-container -->