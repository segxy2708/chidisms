<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package pGlider
 */

get_header();
$quadbox_toggle = get_theme_mod('purelyglider_quadbox_toggle');
$quadbox_types = get_theme_mod('purelyglider_quadbox_types');
$category_toggle = get_theme_mod('purelyglider_category_toggle');
$category_types = get_theme_mod('purelyglider_category_types');
$category_amount = get_theme_mod('purelyglider_category_amount');
$category_desc = get_theme_mod('purelyglider_category_descr');
 ?>

<!-- Start Content -->
	<?php if ($quadbox_toggle == 'show') { ?>
		<div class="custom-box-container">
			<div id="custom-box" class="col grid_12_of_12">
				<div class="custom-box">
				<?php 
					$args = array(
					'post_type' => 'product',
					'posts_per_page' => '4',
					'ignore_sticky_posts' => 'true',
					'meta_query'     => array()
					);
				
					$args['meta_query']   = array_filter( $args['meta_query'] );
					switch ( $quadbox_types ) {
			
					case 'featured' :
					$args['meta_query'][] = array(
					'key'   => '_featured',
					'value' => 'yes'
					);
					break;
					case 'onsale' :
					$product_ids_on_sale    = wc_get_product_ids_on_sale();
					$product_ids_on_sale[]  = 0;
					$args['post__in'] = $product_ids_on_sale;
					break;
					}	
				?>
				<?php $the_query = new WP_Query( $args ); ?>
				<?php if ( $the_query->have_posts() ) : ?>
				<ul class="product_list_widget">
				
				<!-- the loop -->
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<li>
						<a href="<?php echo esc_url( get_permalink( $product->id ) ); ?>" title="<?php echo esc_attr( $product->get_title() ); ?>">
							<?php echo $product->get_image(array(550,550)); ?>
						</a>
						<div class="boxed-layout">
							<a href="<?php echo esc_url( get_permalink( $product->id ) ); ?>" title="<?php echo esc_attr( $product->get_title() ); ?>">
								<span class="product-title"><?php echo $product->get_title(); ?></span>
							</a>
							<div class="price-stuff">
								<?php if ( ! empty( $show_rating ) ) echo $product->get_rating_html(); ?>
								<?php echo $product->get_price_html(); ?>
							</div>
							<?php
								if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
									global $post;
								if ( ! $post->post_excerpt ) return;
							?>
						<?php if ( is_front_page() ) { ?>	
						<div id="description">
								<?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ) ?>
						</div>
						<?php } ?>
						</div>
					</li>
				<?php endwhile; ?>
				<!-- end of the loop -->
				</ul>
				</div>
					<?php wp_reset_postdata(); ?>
					<?php else : ?>
						<p><?php _e( 'Sorry, no posts matched your criteria.', 'emporium' ); ?></p>
					<?php endif; ?>
			</div><!-- #custom-box -->
		</div><!-- .custom-box-container --> 
		<?php } ?>
		

	<?php if ($category_toggle == 'show') { ?>
		<div class="product-category-list">
			<?php 
				$args = array(
					'post_type' => 'product',
					'posts_per_page' => $category_amount,
					'ignore_sticky_posts' => 'true',
					'product_cat' => $category_types,
					);
				$the_query = new WP_Query( $args );
				if ( $the_query->have_posts() ) : 
			?>
				
			<ul class="category-list">
				
				<!-- the loop -->
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<li>
						<a href="<?php echo esc_url( get_permalink( $product->id ) ); ?>" title="<?php echo esc_attr( $product->get_title() ); ?>">
							<?php echo $product->get_image(array(200,200)); ?>
						</a>
						<div class="product-content">
							<a href="<?php echo esc_url( get_permalink( $product->id ) ); ?>" title="<?php echo esc_attr( $product->get_title() ); ?>">
								<span class="product-title"><?php echo $product->get_title(); ?></span>
							</a>
							<div class="product-price">
								<?php if ( ! empty( $show_rating ) ) echo $product->get_rating_html(); ?>
								<?php echo $product->get_price_html(); ?>
							</div>
							<?php
								if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
									global $post;
								if ( ! $post->post_excerpt ) return;
							?>
							<?php if ($category_desc == 'show') { ?>
							<div class="product-description">
							<?php $excerpt = apply_filters( 'woocommerce_short_description', $post->post_excerpt );
								echo substr($excerpt,0, 140);
								echo "..";
							?>
							</div>
							<?php } ?>
						</div>
					</li>
				<?php endwhile; ?>
				<!-- end of the loop -->
			</ul>
		</div>
		<?php wp_reset_postdata(); ?>
		<?php else : ?>
			<p><?php _e( 'Sorry, no posts matched your criteria.', 'emporium' ); ?></p>
		<?php endif; ?>
		
	<?php } ?>		
<!-- End Content -->	
 

<div id="primary" class="content-area">

	<main id="main" class="site-main" role="main">		
		
		<?php if ( have_posts() ) : ?>

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
					/* Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'content', get_post_format() );
				?>

			<?php endwhile; ?>

			<?php purelyglider_the_posts_navigation(); ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>
		


	</main><!-- #main -->
</div><!-- #primary -->
<?php	get_sidebar(); ?>
<?php get_footer(); ?>
