<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package pGlider
 */
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info container">
			<a href="<?php echo esc_url( __( 'http://wordpress.org/', 'purelyglider' ) ); ?>"><?php printf( __( 'Proudly powered by %s', 'purelyglider' ), 'WordPress' ); ?></a>
			<span class="sep"> | </span>
			<?php _e( 'Theme:', 'purelyglider' ); ?>
			<a href="<?php echo esc_url( __('http://www.purelythemes.com', 'pGlider'));?>" target="_blank">pGlider</a>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
