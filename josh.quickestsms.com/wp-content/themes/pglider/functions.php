<?php
/**
 * pGlider functions and definitions
 *
 * @package pGlider
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	global $content_width;
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'purelyglider_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function purelyglider_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on pGlider, use a find and replace
	 * to change 'purelyglider' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'purelyglider', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'purelyglider' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );
	/**
	* Adding support for the post thumbnails
	*/
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 370, 253, true );
	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'purelyglider_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // purelyglider_setup
add_action( 'after_setup_theme', 'purelyglider_setup' );

add_action( 'after_setup_theme', 'purelyglider_woocommerce_support' );
function purelyglider_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function purelyglider_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'purelyglider' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	}
add_action( 'widgets_init', 'purelyglider_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function purelyglider_scripts() {
	wp_enqueue_style( 'purelyglider-style', get_stylesheet_uri() );

	// Google font loading
	wp_enqueue_style( 'purelyglider-googlefont-raleway', "http://fonts.googleapis.com/css?family=Raleway");
	// One more - they are so pretty!
	wp_enqueue_style( 'purelyglider-googlefont-pacifico', "http://fonts.googleapis.com/css?family=Pacifico");
	// Load custom css defining responsivity, grids etc.
	wp_enqueue_style( 'purelyglider-customcss', get_template_directory_uri() . '/css/custom.css' );
	// Load jQuery using WordPress
	wp_enqueue_script( 'jquery' );
	// Loading jQuery Slider Script
	wp_enqueue_script( 'purelyglider-slider', get_template_directory_uri() . '/js/featured-products-slider-script.js', array(), '20150202', true );
	// Load tinynav library
	wp_enqueue_script( 'purelyglider-navigation', get_template_directory_uri() . '/js/tinynav.js', array(), '20150202', true );
	// Execute script and set div selector class
	wp_enqueue_script( 'purelyglider-loadnavigation', get_template_directory_uri() . '/js/tinynav_load.js', array(), '20150202', true );
	// Load als slider library
	wp_enqueue_script( 'purelyglider-slider', get_template_directory_uri() . '/js/jquery.als-1.7.js', array(), '20150202', true );
	wp_enqueue_script( 'purelyglider-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'purelyglider_scripts' );

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

require get_template_directory() . '/inc/purelyglider_custom.php';